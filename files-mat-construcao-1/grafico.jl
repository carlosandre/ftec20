function grafico()

	det1 = [ 4.8 1.3
			 2.4 5
			 1.2 20.5
			 0.6 130.3
			 0.3 415.9
			 .15 340.9
			 .01 109.2 ]

	det2 = [ 4.8 0.9
			 2.4 4.4
			 1.2 21.1
			 0.6 134.3
			 0.3 477.3
			 .15 321
			 .01 121 ]

	ret1 = det1; ret2 = det2;

	for i in 1:7
		ret1[i,2] = sum(det1[:,2]) - sum(det1[1:i,2])
		ret2[i,2] = sum(det2[:,2]) - sum(det2[1:i,2])
	end

	detM = (det1 .+ det2) ./ 2
	retM = (ret1 .+ ret2) ./ 2

	plot(title="Curva Granulométrica (slide 18)", xlabel="Abertura das peneiras (mm, escala log10)", ylabel="Massa acumulada (g)", xflip=true, xscale=:log10)
	xticks!(retM[:,1], string.(retM[:,1]), tickfontrotation=90)
	plot!(ret1[:,1], ret1[:,2], label="Amostra 1")
	plot!(ret2[:,1], ret2[:,2], label="Amostra 2")
	plot!(retM[:,1], retM[:,2], label="Média", linestyle=:dash)
	savefig("/tmp/grafico1.pdf")

	brita1 = [ 
			   32 0
			   25 0
			   19 570.1
			   12.5 429
			   9.5 0
			   6.3 0
			   ]

	brita2 = [
			   32 0
			   25 0
			   19 506.4
			   12.5 403.4
			   9.5 9
			  6.3 0 ]

	ret1 = brita1; ret2 = brita1;

	for i in 1:6
		ret1[i,2] = sum(brita1[:,2]) - sum(brita1[1:i,2])
		ret2[i,2] = sum(brita2[:,2]) - sum(brita2[1:i,2])
	end

	detM = (det1 .+ det2) ./ 2
	retM = (ret1 .+ ret2) ./ 2

	plot(title="Curva Granulométrica (slide 21) - Brita 1", xlabel="Abertura das peneiras (mm)", ylabel="Massa acumulada (g)", xflip=true)
	xticks!(retM[:,1], string.(retM[:,1]), tickfontrotation=90)
	plot!(ret1[:,1], ret1[:,2], label="Amostra 1")
	plot!(ret2[:,1], ret2[:,2], label="Amostra 2")
	plot!(retM[:,1], retM[:,2], label="Média", linestyle=:dash)
	savefig("/tmp/grafico2.pdf")
	
	areia = [ 6.3 2
			  4.8 3
			  2.4 12
			  1.2 17
			  0.6 19
			  0.3 28
			  .15 17
			  .10 3
			  ]

	retM = areia
	for i in 1:6
		retM[i,2] = sum(areia[:,2]) - sum(areia[1:i,2])
	end

	plot(title="Curva Granulométrica - Areia (slide 27)", xlabel="Abertura das peneiras (mm, escala log10)", ylabel="Massa acumulada (\\%)", xflip=true, xscale=:log10)
	xticks!(retM[:,1], string.(retM[:,1]), tickfontrotation=90)
	plot!(retM[:,1], retM[:,2], label="")
	savefig("/tmp/grafico3.pdf")

end
