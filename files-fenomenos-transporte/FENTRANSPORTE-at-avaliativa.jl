### A Pluto.jl notebook ###
# v0.11.10

using Markdown
using InteractiveUtils

# ╔═╡ e82d27e0-e998-11ea-0a58-a198e263c60a
begin
	using Unitful
	using SymEngine
end

# ╔═╡ 1b1eefee-e999-11ea-19ad-39aa2540a1ff
# constantes
begin
	g = 9.80665u"m/s^2"
	ρ_água = 1000u"kg/m^3"
	μ_água = 1.002e-3u"Pa*s" |> upreferred
end;

# ╔═╡ c617c4da-eae2-11ea-06ce-0f859fa8d2b4


# ╔═╡ d1557108-e998-11ea-2dcb-197b3c3fb013
# 01
let
	A1 = 100u"cm^2"		 |> u"m^2"
	p1 = g*0.5u"kg/cm^2" |> u"Pa"
	h1 = 100u"m"		 |> u"m"
	
	A2 = 50u"cm^2"		 |> u"m^2"
	p2 = g*3.38u"kg/cm^2"|> u"Pa"
	h2 = 70u"m"			 |> u"m"
	
	μ = ρ_água
	
	@vars v1
	v2 = 2*v1
	
	# b1 = b2
	b1 = p1 + μ/2 * (v1*u"m/s")^2 + μ*g*h1 |> u"Pa"
	b2 = p2 + μ/2 * (v2*u"m/s")^2 + μ*g*h2 |> u"Pa"
	
	# 1029698.25 - 1017930.27 = 2000.0*v1^2 - 500.0*v1^2 
	# 11767.979999999981 = 1500.0*v1^2
	v1 = sqrt(11767.979999999981/1500.0) * u"m/s"
	
	Q = A1*v1 |> u"L/s"
	# 0.0280094983889394 m³/s ou 28.0094983889394 L/s
	@show(v1, Q)
end

# ╔═╡ 639002fa-e99d-11ea-3956-09ed182d9429
# 02
let
	Q  = 6.35u"L/s"	|> upreferred
	
	D1 = 63.5u"mm"	|> upreferred
	A1 = π*D1^2/4	|> upreferred
	L1 = 50u"m"		|> upreferred
	v1 = Q / A1		|> upreferred
	
	D2 = 50u"mm"	|> upreferred
	A2 = π*D2^2/4	|> upreferred
	v2 = Q / A2		|> upreferred
	@show A2 v2
	
	# considerando hp1 = hp2
	
	# a) 127069.81484382859
	Re1 = ρ_água * v1 * D1 / μ_água

	
	# b) 35.54827122646933 m
	ϵ1  = 0.0000015u"m" # PVC
	kD1 = ϵ1 / D1 # 2.362204724409449e-5
	f1 = 0.017268205190186407 # http://www.advdelphisys.com/michael_maley/moody_chart/
	hp = f1*(L1*v1^2)/(D1*2g)
	
	ϵ2  = 0.0001220u"m" # Ferro Galvanizado rev. de Asfalto
	kD2 = ϵ2 / D2 # 0.00244
	Re2 = ρ_água * v2 * D2 / μ_água # 161378.66485166232
	f2  = 0.02566298569921762 # http://www.advdelphisys.com/michael_maley/moody_chart/
	# [ hp == L2 * (f2 * v2^2)/(D2 * 2g) ]
	L2 = hp / ((f2 * v2^2)/(D2 * 2g)) # 10.18336752747795 m

	# c) Turbulento  [ Re2 = 161.378,66 > 2400 ]
	Re1
end

# ╔═╡ f81f953c-ea49-11ea-2e28-d18a93fc21fd
# 03
let
	dmdt = 3600u"kg/hr"
	H_entr = 3276.6u"kJ/kg"
	H_sai  = 2780.1u"kJ/kg"
	
	# du/dt = sum(dot(m) * (h+Ec+Ep)) - sum(dot(m) * (h+Ec+Ep)) + Q + W
	ΔH = H_entr - H_sai
	
	ΔH * dmdt |> u"kW"
end

# ╔═╡ eaf5d1ce-eb1a-11ea-2db3-47b940d649b9


# ╔═╡ e6084da0-eae7-11ea-3080-152da79a352c


# ╔═╡ Cell order:
# ╠═e82d27e0-e998-11ea-0a58-a198e263c60a
# ╟─1b1eefee-e999-11ea-19ad-39aa2540a1ff
# ╠═d1557108-e998-11ea-2dcb-197b3c3fb013
# ╠═639002fa-e99d-11ea-3956-09ed182d9429
# ╠═f81f953c-ea49-11ea-2e28-d18a93fc21fd
# ╠═eaf5d1ce-eb1a-11ea-2db3-47b940d649b9
# ╠═e6084da0-eae7-11ea-3080-152da79a352c
