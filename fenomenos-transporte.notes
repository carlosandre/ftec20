: title = "Fenômenos de Transporte"

# Introdução

O estudo de mecânica dos fluidos, de transmissão de energia e transferência de
massa é denominado Fenômenos de Transporte. Normalmente, estes fenômenos são
desenvolvidos independentemente como ramos da Física Clássica, no entanto, na
engenharia seu estudo é unificado, já que quase sempre acontecem em conjunto.

O principal objeto de estudo desta disciplina são os aspectos relacionados ao
movimento dos fluidos (líquidos e gases) e algumas de suas características
específicas. A ideia do estudo do movimento já existe há muito tempo (Heráclito
de Éfeso -- tudo flui, tudo se movimenta), e, a partir do estudo da disciplina,
serão mostradas formas de avaliar e quantificar fenômenos de vazão, perda de
carga, transferência de calor, entre outros.

A transferência de matéria e energia ocorre na existência de uma diferença de
pressão, temperatura ou concentração, através de mecanismos de transporte,
compostos de quantidades de movimento, calor e massa, por meios moleculares,
convectivos ou de radiação.



# Deformação e Escoamento

Neste contexto, pode-se perceber uma diferença importante no comportamento
entre sólidos e fluidos, quando sob deformação: a deformação de um sólido
relaciona-se com o Módulo de Rigidez (ou módulo de torção), que determina uma
característica elástica. Por outro lado, fluidos não podem ser considerados
elásticos, e a grandeza relacionada a sua deformação é a viscosidade, que
representa a dificuldade de escoamento desse fluido.

Deste modo, define-se como fluido uma substância que se deforma continuamente
sob ação de uma tensão de cisalhamento tangencial. A tensão $sigma é uma força $F
aplicada sobre uma determinada área $A, já a tensão de cisalhamento pode ser
exemplificada através da representação de um fluido entre duas placas
infinitas, onde há uma força sendo aplicada na placa superior:

$ σ == F / A
$ τ_yx == ∂F_x / ∂A_y
  τ_yx: tensão de cisalhamento [Pa]
  ∂F_x: força tangencial aplicada [N]
  ∂A_y: área de contato [m^2]

Define-se como taxa de deformação (ou taxa de cisalhamento) a diferença de
ângulo $alpha provocada por esta tensão, em função do tempo $t:

$ "taxa de deformação" == lim(∂α / ∂t, ∂t=>0) == d(α) / d(t)

Através de relações (aproximação trigonométrica) entre os valores, tem-se que:
% para ângulos pequenos (diferencial), sin(x) == x

$ ∂α / ∂t == ∂u / ∂y
  α: ângulo de deformação do fluido (ver imagem)
  t: tempo [s]
  u: velocidade de escoamento [m/s]
  y: distância entre as placas [m]

Considera-se como _fluidos newtonianos_ aqueles em que a taxa de deformação é
proporcional à tensão de cisalhamento, ou seja, aqueles em que a relação a
seguir é verdadeira:

$ τ_yx ∝ d(u) / d(y)



# Viscosidade e Número de Reynolds

A constante de proporcionalidade da equação anterior é a viscosidade dinâmica
($mu), e sua unidade é $[u"N*s/m^2"] ou $[u"Pa*s"], tal que:

$ τ_yx == μ * ( d(u) / d(y) )

A viscosidade cinemática é definida como a razão entre viscosidade dinâmica e
massa específica ($[γ=μ/ρ]), sendo representada pelas letras $ν ou $γ, com a
unidade $[u"m^2/s"]. É importante verificar a diferença entre estas duas formas
de viscosidade em seu estudo.

A viscosidade varia de acordo com a temperatura. Em líquidos, quanto maior a
temperatura, menor é a viscosidade, enquanto que, em gases, a viscosidade
aumenta com o aumento da temperatura.

O escoamento pode ser classificado em laminar ou turbulento, através da
determinação do seu número de Reynolds. Ele é obtido através da equação:

$ Re == ρ * v * D / μ
  Re: número de reynolds
  ρ: massa específica [kg/m^3]
  v: velocidade do fluido [m/s]
  D: diâmetro [m]
  μ: viscosidade dinâmica [Pa*s]

Normalmente, valores superiores a 2000 configuram escoamento turbulento, caso
contrário, o escoamento é laminar. Algumas literaturas definem este valor como
2500.



# Pressão Hidrostática

É a pressão que existe no interior de um líquido em repouso. A pressão, em
geral, é a força que atua sobre uma determinada área, cuja unidade é o Pa
(Pascal):

$ p == F/A
  p: pressão [Pa]
  F: força [N]
  A: área [m^2]

Já a pressão hidrostática pode ser representada através da equação

$ p == ρ*g*h
  ρ: densidade do líquido [kg/m^3]
  g: aceleração da gravidade [m/s^2]
  h: altura de fluido sobre o ponto analisado [m]



## Teoremas e Princípios

O Teorema de Stevin leva em consideração a pressão atmosférica, que também é
exercida sobre qualquer ponto em um fluido. Se o ponto estiver sobre a
superfície do líquido, a pressão será igual à exercida pelo gás sobre ela,
pontos a uma mesma altura estão sujeitos à mesma pressão, e a superfície livre
dos líquidos está sempre no mesmo nível horizontal.

Já o Princípio de Pascal determina que um aumento ou diminuição de pressão em
um ponto de um fluido será igualmente distribuído em todos os seus pontos,
normalmente exemplificado por êmbolos:

$ F[1] / A[1] == F[2] / A[2]

O princípio de Arquimedes estabelece que o empuxo ao qual um corpo é submetido
equivale ao peso do fluido deslocado, ou seja:

$ E = m_f * g
  E:   empuxo [N]
  m_f: massa do fluido [kg]
  g:   aceleração da gravidade [m/s^2]

De modo a simplificar a aplicação destes conceitos, utiliza-se a equação
manométrica, na qual são comparados os pesos específicos e altura de cada
líquido em um sistema, conforme o exemplo:

$ p[A] + gamma[1]*h[1] + gamma[2]*h[2] + dots() == p[B]

!img manometrica.jpg



# Vazão

É uma grandeza que determina o volume de fluido que atravessa uma seção
transversal por unidade de tempo, podendo ser representada pelos símbolos $Q
ou $Z. No Sistema Internacional de unidades, a unidade da vazão é $[u"m^3/s"],
mas também são utilizadas unidades como litro por segundo ou hora, entre
outros.

$ Q = V / Δt
  Q: vazão [m^3/s]
  V: volume [m^3]
  Δt: intervalo de tempo [s]



## Equação da Continuidade

A Equação da Continuidade relaciona velocidade de escoamento de um fluido e a
área de seção transversal disponível. Adotando o fluido analisado como
incompressível, ou seja, um fluido em que a densidade não depende da pressão,
tem-se que:

$ Q = A * v
  Q: vazão [m^3/s]
  A: área da seção transversal [m^2]
  v: velocidade [m/s]

Considerando que a vazão de entrada em um sistema deverá ser igual à sua vazão
de saída, pode-se concluir que:

$ Q[1] == Q[2]
$ A[1] * v[1] == A[2] * v[2] == A[3] * v[3] == Q

# Princípios da Conservação

Nesta disciplina, estuda-se os princípios de conservação de massa, quantidade
de energia e movimento. Quando se relaciona a conservação de massa, a
quantidade de massa que entra em um sistema é a mesma que sai dele, na
conservação de movimento, relaciona-se as forças envolvidas, e de energia, está
relacionada a Primeira Lei da Termodinâmica.

A análise destes fenômenos se dá através de volumes e superfícies de controle.
Em termos de volume de controle (VC), este é arbitrário, e escolhido de modo a
envolver a estrutura de interesse. A superfície de controle (SC) a ele
relacionada é composta de todas as fronteiras deste volume. Por exemplo,
pode-se observar entrada, saída, variação ou não de volume. Em outras palavras,
os sistemas de controle representam e delimitam os objetos de análise e estudo
de um fenômeno.

Quando trabalhamos em um sistema, observa-se uma variação em função do tempo de
uma propriedade extensiva: massa, movimento ou energia, representada por:

$ diff(N, t)

A equação geral que representa estas variações é

$ diff(N, t) ==
$	diff(_,t) * int(eta*rho, V, "VC", _) +
$	int(eta * rho * vec(v) * vec(n), A, "S1", _) +
$	int(eta * rho * vec(v) * vec(n), A, "S2", _) + dots()
eta: depende do tipo de conservação
rho: massa específica [kg/m^3]
V: volume [m^3]
vec(v): velocidade [m/s]
vec(n): direção (para fora)
A: área de seção [m^2]

O vetor $[vec(n)] tem magnitude 1 sempre aponta para fora do sistema, ou seja,
torna positivos os valores de saída, e negativos os valores de entrada. É
importante notar que diversos dos valores na integral são constantes, e,
portanto, podem ser "tirados" da integral, já que a integral de uma constante
equivale à própria constante.  As variáveis $N e $eta diferem de acordo com a
propriedade analisada:

| Lei                     | $N        | $eta
  Conservação de Massa    | $M        | $[1]
  2a Lei de Newton        | $[vec(p)] (Pressão) | $[vec(v)] (Velocidade)
  1a Lei da Termodinâmica | $E        | $[e &" (Bernoulli)"]



## Conservação de Energia

A primeira lei da Termodinâmica determina, baseando-se na conservação da
energia, diz que energia não pode ser criada ou destruída. Em outros termos,
pode-se dizer que:

$ ΔE = Q - W
E: energia
Q: quantidade de calor
W: trabalho

Em um fluido escoando em regime permanente, tem-se em $ΔE as seguintes
energias:

- Energia Potencial Gravitacional: $[m*g*h]
- Energia Cinética: $[(1/2)*m*v^2]
- Energia por Diferencial de Pressão
- Energia Dissipada por Atrito (perda de carga, por exemplo)
- Energia Molecular (energia interna)

Um sistema pode ser aberto, fechado ou isolado. No caso do sistema aberto,
existe troca de massa e energia (ex. copo d'água), enquanto que em um sistema
fechado apenas existe troca de energia (ex. garrafa d'água). Um sistema isolado
não troca nem massa, nem energia (ex. geladeira).

A energia interna é representada pelo símbolo $U, e é a soma das $E_c (energia
cinética) e $E_p (energia potencial) das moléculas que compõem o sistema.

Já o calor, representado por $Q, é a energia em trânsito, que flui em função de
uma diferença de temperatura. Neste caso, pode-se considerar processos como
endotérmicos ou exotérmicos.  Quando se cede calor para um gás, este tende a
expandir. No caso de líquidos e sólidos, ocorre uma mudança de temperatura,
mantendo volume constante. Um processo sem trocas de calor é chamado de *adiabático*.

O trabalho ($W) é a energia que flui como resposta a uma força motriz, que não
uma diferença de temperatura. Normalmente, está associado a algo útil,
realizado sobre ($[W<0]) ou pelo ($[W>0]) sistema. A energia de um sistema pode
ser considerada como a sua capacidade de efetuar trabalho.

O balanço de energia em sistemas abertos pode ser determinado por:

$ par(diff(u, t), sub="sistema") ==
$	sum(dot(m) * (h + e_p + e_c), sub="saída") -
$	sum(dot(m) * (h + e_p + e_c), sub="entrada") +
$	dot(q) + dot(w_e)
diff(u, t): diferencial de energia [J/s]
dot(m): diferencial de massa [kg/s]
h: entalpia [kJ/kg]
e_p: energia potencial [kJ/kg]
e_c: energia cinética [kJ/kg]
dot(q): diferencial de calor [J/s]
dot(W): diferencial do trabalho [J/s]

Caso o processo seja isotérmico, não existe variação de energia interna
($[Δh = 0]), caso seja adiabático, não existe trocas de calor ($[dot(q) = 0]),
se não existirem partes móveis (motores ou turbinas), não existe potência
produzida ou consumida ($[dot(W) = 0]).

### Equipamentos de Controle do Fluxo

*Bocal:* diminui a velocidade do escoamento, reduzindo sua pressão. Atua em
regime permanente, não realiza trabalho nem trocas de calor, e diminui a seção
do conduto;

*Difusor:* aumenta a pressão do escoamento, reduzindo sua velocidade. Da mesma
forma, opera em regima permanente, e não realiza trabalho nem trocas de calor.
Aumenta a seção do conduto;

*Bomba Hidráulica:* transfere energia para um líquido, produzindo aumento da
pressão. Existe trabalho sendo realizado sobre o fluido, mas energia cinética,
potencial e trocas de calor normalmente são desprezíveis;

*Compressor:* um tipo específico de bombas que apresenta a função primária de
aumentar a pressão de gases;

*Turbina:* é um dispositivo pelo qual o fluido realiza trabalho, sobre pás que
giram. Neste caso, há uma queda de energia no fluido, transferida para o meio
externo como trabalho útil. Energia cinética, potencial e trocas de calor
normalmente são desprezíveis;

*Válvulas de Estrangulamento:* promove a redução de pressão sem variação na
energia potencial ou cinética, em regime permanente e sem realização de
trabalho;

*Trocadores de Calor:* têm como função aquecer ou resfriar fluidos, podendo
haver ou não mudança de fase. Este processo ocorre em regime permanente sem
realização de trabalho. A transferência de calor é significativa;

### Equação de Bernoulli

O Princípio de Bernoulli afirma que, para um fluxo sem viscosidade, um aumento
na velocidade do fluido ocorre simultaneamente a uma diminuição na pressão ou
energia potencial do fluido. Considerando o Teorema do Trabalho:

$ W == ΔK
W: trabalho
K: energia cinética

$ ΔK == E_c[1] - E_c[0] == (m*v[1]^2/2) - (m*v[0]^2/2)
$ m == mu * ΔV
m: massa
mu: massa específica
V: volume
$ ΔK == mu * ΔV/2 * (v[1]^2 - v[0]^2)

$ W == W_g + W_s
W_g: trabalho da força gravitacional
W_s: trabalho do sistema
$ W_g == -m * g * Δh
$ W_s == p[1] * ΔV - p[2] * ΔV
m: massa
g: aceleração da gravidade
Δh: diferença de altura
p: pressão

Juntando-se todos os termos, tem-se:

$ p[0] + mu/2 * v[0]^2 + mu * g * h[0] == p[1] + mu/2 * v[1]^2 + mu * g * h[1] 

Pode-se, também, utilizar uma versão simplificada da equação de Bernoulli,
quando se considera as restrições a seguir:

$ h[1] + p[1]/gamma + v[1]^2/2g ==
$     h[2] + p[2]/gamma + v[2]^2/2*g == "constante"
h: altura [m]
p: pressão [Pa]
gamma: peso específico do fluido [N/m^3]
v: velocidade do fluido [m/s]
g: aceleração da gravidade [m/s^2]

ou

$ under(h[1], "carga potencial (altimétrica)") +
$	under(p[1]/gamma, "carga de pressão (piezométrica)") +
$	under(v[1]^2/2g, "carga de velocidade (cinética)") == "constante"

- regime permanente
- escoamento incompressível
- escoamento sem atrito
- escoamento ao longo de uma linha de corrente
- ausência de trabalho de eixo entre 1 e 2,
- ausência de trocas de calor entre 1 e 2.


Quando na situação analisada estiver envolvida uma máquina, como uma bomba
($[W>0]) ou turbina ($[W<0]), por exemplo, utiliza-se a equação de balanço de
energia mecânica, acrescentando a diferença de energia em função da bomba. De
modo a selecionar esta bomba, aplica-se no sistema analisado o trabalho
agregado. É importante também considerar a eficiência da bomba, normalmente
representada pelo símbolo $eta.

A equação de Bernoulli também pode ser utilizada para determinar a perda de
carga em um sistema:

$ h[1] + p[1]/gamma + v[1]^2/2g ==
$     h[2] + p[2]/gamma + v[2]^2/2*g + ΔH
ΔH: perda de carga



# Perda de Carga

Carga é definida como a relação entre a energia de um fluido por seu peso.
Fatores como o atrito e presença de conexões e outros acessórios em uma
tubulação estão agregados na perda de carga.  Em outras palavras, a perda de
carga é equivalente à energia perdida em um tubo ou acessório, dissipada em
forma de calor ou de turbilhões formados em uma corrente fluida, em ambos os
casos tendo impacto relacionado à rugosidade e outras características do
conduto.

Normalmente, a perda de carga é representada pelo símbolo $h ou $h_p, e sua
dimensão é linear.  Quando a equação de Bernoulli é aplicada a dois pontos de
um conduto com velocidade constante e mesma cota, tem-se que a perda de carga
equivale a:

$ h_p = (p[1] - p[2]) / gamma

No entanto, no estudo de Fenômenos de Transporte aplica-se a equação de Darcy:

$ h_p = f*((L*v^2)/(D * 2g))
f: coeficiente de atrito (diag. Moody)
L: comprimento da tubulação [m]
v: velocidade do fluido [m/s]
D: diâmetro da tubulação [m]
g: aceleração da gravidade [m/s^2]

Para utilizar o Diagrama de Moody, precisa-se da rugosidade do material e o
número de Reynolds a ser considerado.  Também deve-se diferenciar rugosidade
absoluta e relativa. A aspereza das paredes do tubo, por menor que esta seja,
acaba por ser uma das responsáveis pela perda de carga, e a medida desta
rugosidade é representada pela rugosidade absoluta ($[ϵ]). A rugosidade
relativa equivale à divisão entre a rugosidade absoluta e o diâmetro do tubo:

$ "Rugosidade Relativa" = ϵ / D

!img rugosidade.png

!img rugosidade-tabela.png

!img moody.png

Perda de carga localizada ou singular é considerada em trechos específicos do
conduto, como junções, derivações, curvas, válvulas, entradas e saídas, entre
outros. Pode ser calculado pelo coeficiente de perda de carga ou por seu
comprimento equivalente. A expressão geral da perda de carga localizada é:

$ h_p = k * (v^2/2g)
k: coeficiente determinado experimentalmente
v: velocidade do fluido [m/s]
g: aceleração da gravidade [m/s^2]

% http://www.advdelphisys.com/michael_maley/moody_chart/

# Transferência de Calor

Calor ou transferência de calor é a energia térmica transportada de ou para um
sistema em função de uma diferença de temperatura no espaço. A temperatura de
um corpo está relacionada a seu nível de vibração molecular. Esta transferência
de energia pode ocorrer de três maneiras: a condução, convecção e radiação.

O fluxo de calor sempre ocorre do material mais quente para o menos quente, e
pode ser determinado por:

$ Phi = q / Δt


% fluxo vs taxa de calor

## Condução

A condução ocorre predominantemente em sólidos, mesmo que também ocorra menos
significativamente em fluidos em repouso. Seu funcionamento ocorre a partir da
vibração de uma molécula, que acaba por agitar as moléculas vizinhas. Neste
contexto, é importante a existência de um meio interveniente, ao contrário da
radiação, mas não requer uma movimentação macroscópica de massa. Pode ser
descrita pela Lei de Fourier:

$ q_x = -k * A * diff(T,x)
q: taxa de calor [W]
k: condutividade térmica [W/(m*K)]
A: área [m^2]
diff(T, x): gradiente de temperatura [K/m]

A taxa de transferência de calor é igual por toda a espessura do material, e, portanto, o
gradiente de temperatura é linear. A partir disto, pode-se determinar que:

$ diff(T, x) = ΔT / Δx

Como o fluxo de calor sempre se dá do quente para o frio, a diferença de
temperatura *sempre será negativa*. Deste modo, a taxa de transferência de
calor será sempre positiva, em função do sinal negativo da fórmula.

É possível elaborar uma relação entre resistência térmica e elétrica. Quando
houver mais de uma interface com materiais diferentes, as relações (em série,
em paralelo) são similares.

$ q = ΔT / R_t
q: fluxo de calor
ΔT: diferença de temperatura
R_t: resistência térmica [K/W]

$ R_t = L/(k*A)
R_t: resistência térmica [K/W]
L: espessura [m]
k: condutividade térmica [W/(m*K)]
A: área transversal ao fluxo [m^2]

Por exemplo, se houverem paredes em série, o valor das resistências é somado:

$ q = ΔT / sum(R_t) ==  ΔT / sum(L/(k*A))

Neste contexto, existem casos especiais:

*Condução entre a parte interna de um cilindro e o meio exterior:*

$ R_t = log(r_2 - r_1) / (2*π*L*k)

$ q = (2*π*L*k) * (T_1-T_2) / log(r_2 - r_1)

!img cond-cilindros.png

*Condução entre a parte interna de um cilindro composto de dois materiais e o meio exterior:*

$ q = (T_1 - T_2) / (
$	1 / (2*π*r_1*L*h_1)
$ + log(r_2/r_1) / (2*π*L*k_1)
$ + log(r_3/r_2) / (2*π*L*k_2)
$ +	1 / (2*π*r_3*L*h_2)
$)

 *Condução entre a parte interna de uma esfera oca e o meio exterior:*

$ R_t = 1/(4*π*k) * (1/r_1 - 1/r_2)

$ q = (T_1 - T_2) / ( 1/(4*π*k) * (1/r_1 - 1/r_2) )


## Convecção

A convecção ocorre em fluidos, líquidos ou gasosos, e envolve o deslocamento de
massa de modo a transferir o calor. Quando este fluido escoa sobre uma
superfície sólida que possua temperatura diferente, ocorre uma transferência de
calor por condução entre fluido e superfície, que acaba por movimentar o fluido
em relação à superfície. Assim como a condução, é necessário um meio
interveniente para a propagação do calor.  A convecção pode ser natural ou
forçada.

A convecção pode ser natural ou forçada. No primeiro caso, o movimento ocorre
devido a uma diferença de peso específico entre os fluidos quente e frio,
enquanto que no segundo, o movimento ocorre por um mecanismo externo
(ventiladores, secadores de cabelo, entre outros).

Este mecanismo é descrito pela Lei de Resfriamento de Newton:

$ q = h*A*(T_w - T_∞)
q: taxa de calor [W]
h: coeficiente de convecção [W/(m^2*K)]
A: área [m^2]
T_w: temperatura da superfície [K]
T_∞: temperatura do fluido [K]

O coeficiente de convecção $h depende de propriedades físicas do fluido, como
sua velocidade, tipo de escoamento, geometria, entre outros. No entanto, são
normalmente utilizados valores tabelados. Assim como na condução, é possível
calcular a resistência térmica para transferência convectiva:

$ R_t = 1/(h*A)

## Radiação

A irradiação térmica, radiação térmica ou radiação de corpo negro é a
transferência de energia térmica através da radiação eletromagnética emitida
por um corpo. Um corpo pode emitir radiação, independentemente do meio onde
está localizado, em outras palavras, independe da existência de um meio
interveniente. Qualquer corpo que possua temperatura acima do zero absoluto
sempre emite radiação térmica.

Enquanto que a palavra radiação está relacionada à energia emitida por um
corpo, a irradiação se refere à exposição de um corpo a esta radiação. Esta
energia normalmente está na faixa infravermelho do espectro eletromagnético,
mas sua emissão passa a ser mais relevante quando entra na faixa da luz
visível (temperaturas acima de $[753u"K"]).

É denominado corpo negro o corpo ideal, cuja superfície é capaz de absorver
toda a radiação incidente, independentemente do comprimento de onda ou direção.
Pode-se dizer, também, que um corpo negro é um emissor perfeito de radiação em
todos os comprimentos de onda e todas as direções. Para uma dada temperatura, o
corpo negro possui a superfície que possa emitir mais energia radiante.  O
poder emissivo de um corpo negro pode ser determinado através da seguinte
equação:

$ E_b = sigma * T^4
E_b: poder emissivo de um corpo negro
sigma: constante de Stefan-Boltzmann
T: temperatura [K]

$ sigma = 5.6697e-8u"W/(m^2*K^4)"
$ sigma = 4.88e-8u"kcal/(hr*m^2*K^4)"

O denominado corpo cinzento é aquele cuja emissividade e absortividade de sua
superfície são independentes do comprimento de onda e direção. A radiação
emitida ou refletida por um corpo cinzento é considerada difusa. A emissividade
e absortividade de um corpo cinzento são iguais.

Os corpos reais são diferentes dos corpos negro e cinzento. A radiação emitida
por um corpo real não é completamente difusa, e, portanto, depende do ângulo de
observação.

Superfícies reais emitem uma fração da radiação de um corpo negro. Neste
contexto, existe o conceito de emissividade, representado por $e ou $epsilon.
Este valor varia entre 0 e 1, e seus valores na maioria dos casos podem ser
considerados como constantes em função de sua temperatura:

$ epsilon = E_real / E_b

Além da emissividade, trata-se de outras grandezas:

$ epsilon = "emissividade"
$ rho = "refletividade"
$ alpha = "absortividade"
$ tau = "transmissividade"

Além disso, pode-se analisar a interação entre dois corpos que emitem radiação.
O *fator forma* define a quantidade de energia emitida por uma superfície que
será interceptada por uma segunda superfície. O fator forma depende da
geometria relativa dos corpos e de suas emissividades. Pode-se dizer que:

$ A_1 * F_12 = A_2 * F_21
A_1: superfície do corpo 1 [m^2]
F_12: fator forma do corpo 1 com relação ao corpo 2
A_2: superfície do corpo 2 [m^2]
F_12: fator forma do corpo 2 com relação ao corpo 1

Considerando duas superfícies reais:

$ dot(Q) = sigma * F_12 * A_1 (T[1]^4 - T[2]^4)

Caso as superfícies possuam grandes dimensões e sejam corpos
negros, $[F_12 = 1]. No caso de superfícies cinzentas grandes e paralelas:

$ F_12 = 1/(1/e[1] + 1/e[2] - 1)

Para uma superfície cinzenta (1) muito menor que outra superfície cinzenta (2)

$ F_12 = e_1

Assim como nas outras formas de fluxo de calor, a radiação também pode ser
expressa na forma de resistência:

$ R_T = 1/(h_r * A)

## Coeficiente Global de Transferência de Calor

Muitos processos industriais envolvem uma combinação entre diferentes formas de transferência de calor. Para facilitar essa análise, pode-se utilizar o coeficiente global de transferência de calor:

$ q = U * A * ΔT
$ I = 1 / (  1/h[1] + L/kappa + 1/h[2]  )

% slide 53,54,...
