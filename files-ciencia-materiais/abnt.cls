\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{abnt}[05/03/2019 abnt]
\LoadClass[a4paper, 12pt]{article}

\def\curso{Engenharia Civil}
\def\disciplina{Introdução à Engenharia}
\def\autor{} % separados por \par caso haja mais de um
\def\orientador{}
\def\orientadora{}
\def\coorientador{}
\def\coorientadora{}
\def\titulo{}
\def\subtitulo{}
\def\cidade{Caxias do Sul}
\def\ano{\the\year}
\def\resumo{}
\def\palavraschave{}
\def\abstract{}
\def\keywords{}

% Outras Packages e Comandos
\RequirePackage[brazil]{babel}
\RequirePackage{etoolbox,environ}
\newcommand*{\centerfloat}{\parindent \z@ \leftskip \z@ \@plus 1fil \@minus \textwidth \rightskip\leftskip \parfillskip \z@skip}


% Opções
% ---- Fonte: default Palatino/Euler
\newcommand{\optionFont}{}\newcommand{\sftitle}{}
\DeclareOption{times}{\renewcommand{\optionFont}{times,newtxmath}\renewcommand{\sftitle}{\sffamily}}
\DeclareOption{palatino}{\renewcommand{\optionFont}{tgpagella,eulervm}}

\DeclareOption{capa}{\AtBeginDocument{\setlength{\parindent}{0mm}\maketitle}} % espaçamento simples para a capa
\DeclareOption{lof}{\AtBeginDocument{\listoffigures}}
\DeclareOption{lot}{\AtBeginDocument{\listoftables}}
% lista de figuras, gráficos, quadros, tabelas, abreviaturas e siglas, símbolos

\DeclareOption{toc}{\AtBeginDocument{\tableofcontents\cleardoublepage}}

\ProcessOptions


% Alinhamento
\RequirePackage[a4paper, inner=30mm, top=30mm, outer=20mm, bottom=20mm]{geometry}
\RequirePackage{multicol}
\RequirePackage{indentfirst}
\RequirePackage{setspace}
\setlength{\parskip}{1ex plus 1ex minus 0.5ex}
% Tabelas:
\setlength{\tabcolsep}{1em} % horizontal
\renewcommand{\arraystretch}{1.3}% vertical


% Fontes
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{\optionFont}
\RequirePackage{inconsolata}
\RequirePackage{microtype}
\renewcommand\large{\@setfontsize\large{14}{18}}
\renewcommand\small{\@setfontsize\small{10}{13}}


% Figuras e Floats
\RequirePackage{graphicx}
\RequirePackage{caption}
\captionsetup{format=hang,font={bf,small,singlespacing},labelsep=endash,position=above}

\newcommand{\figura}[4][width=0.5\textwidth]{% caminho, legenda, fonte
    \begin{figure}[htbp]%
        \centerfloat\small\medskip%
        \caption{#3}%
        \label{fig:#2}%
        \includegraphics[#1]{#2}%
        \par\vspace{6.5pt}Fonte: 
        \def\param{#4}%
        \ifx\param\empty
            o autor%
        \else
            \citeauthor*{#4} \citeyear{#4}%
        \fi%
    \end{figure}%
}


% Formatação em Geral
\RequirePackage{titlesec,titletoc}
\setcounter{secnumdepth}{4}
\setcounter{tocdepth}   {4}
\newcommand{\sectionbreak}{\cleardoublepage}
\titleformat{\section}
    {\normalsize\sftitle\bfseries}
    {\thesection~}{0em}{\MakeUppercase}
\titleformat{name=\section,numberless}
    {\normalsize\sftitle\bfseries\centering}
    {}{0em}{\MakeUppercase}
\titleformat{\subsection}
    {\normalsize\sftitle}
    {\thesubsection~}{0em}{\MakeUppercase}
\titleformat{\subsubsection}
    {\normalsize\sftitle\bfseries}
    {\thesubsubsection~}{0em}{}
\titleformat{\paragraph}
    {\normalsize\sftitle}
    {\theparagraph~}{0em}{}
% Espaçamento
\titlespacing*{\section}{0em}{0em}{1em}
\titlespacing*{\subsection}{0em}{1.3em plus 1em}{1em}
\titlespacing*{\subsubsection}{0em}{1.3em plus 1em}{1em}
\titlespacing*{\paragraph}{0em}{1.3em plus 1em}{1em}
% Formatação no Sumário
\titlecontents{section}[0em]
    {\vspace{1ex}}
    {\sftitle\bfseries\thecontentslabel~\uppercase}
    {\hspace*{-2em}\uppercase}
    {\titlerule*[.75em]{.}\sftitle\contentspage}
\titlecontents{subsection}[15mm]
    {\vspace{0ex}}
    {\sftitle\thecontentslabel~\uppercase}
    {\hspace*{-2em}\uppercase}
    {\titlerule*[.75em]{.}\sftitle\contentspage}
\titlecontents{subsubsection}[30mm]
    {\vspace{0ex}}
    {\sftitle\bfseries\thecontentslabel~}
    {\hspace*{-2em}\uppercase}
    {\titlerule*[.75em]{.}\sftitle\contentspage}
\titlecontents{paragraph}[45mm]
    {\vspace{0ex}}
    {\sftitle\thecontentslabel~}
    {\hspace*{-2em}\uppercase}
    {\titlerule*[.75em]{.}\sftitle\contentspage}
\RequirePackage{titleps}
\newpagestyle{main}{
    \sethead{}{}{\thepage}
}


% Listas
\renewcommand{\labelenumi}{\alph{enumi}$)$ }
\renewcommand{\labelenumii}{--}
\usepackage{enumitem}
\setlist[enumerate]{leftmargin=1.5\parindent,labelindent=\parindent,labelwidth=0.35\parindent,align=left}


% Bibliografia
\RequirePackage[style=abnt]{biblatex}
\RequirePackage{csquotes}
\SetCiteCommand{\parencite}
\addbibresource{referencias.bib}
\DeclareBibliographyCategory{hidden} % caso seja necessário tirar uma referência

\renewenvironment{quote}{
    \par\setlength{\parindent}{0mm}
    \setlength{\leftskip}{40mm}
    \vspace{10mm plus 5mm minus 5mm}
    \setstretch{1}%
    \small%
}{\par\vspace{10mm plus 5mm minus 5mm}}
\AtEndDocument{\printbibliography[notcategory=hidden]}
% Glossário, apêndices, anexos, índice

\RequirePackage[hidelinks]{hyperref} % precisa ser o último


\renewcommand{\maketitle}{% CAPA
    \cleardoublepage% criar nova página *se houver algo antes*
    \pagestyle{empty}% remover número de página
    % CAPA:
    {\sftitle\bfseries\large\centering%
        \includegraphics[width=8cm]{logo.png}%
        \par\vspace{2.5em}%
        \MakeUppercase{Curso Superior de \curso}%
        \par\vspace{\fill}%
        \MakeUppercase{\autor}%
        \par\vspace{\fill}%
        \MakeUppercase{\titulo}%
        \ifdefempty{\subtitulo}{\relax}{\par\vspace{1ex}\subtitulo}%
        \par\vspace{\fill}%
        \par\vspace{\fill}%
        \cidade\par\ano%
        \par\vspace{1cm}%
        \cleardoublepage%
    }
    % FOLHA DE ROSTO:
    {\sftitle\bfseries%
        {\centering%
            \MakeUppercase{\autor}%
            \par\vspace{\fill}%
            {\large%
                \MakeUppercase{\titulo}%
                \ifdefempty{\subtitulo}{\par}{:\par\vspace{1ex}\subtitulo\par}%
            }%
        }%
        \par\vspace{\fill}%
        {\setlength{\leftskip}{7.5cm}%
            % Remover hífens neste trecho:
            \tolerance=1 \emergencystretch=\maxdimen \hyphenpenalty=10000 \hbadness=10000%
            Trabalho apresentado para o Curso de \curso{}
            do Centro Universitário Uniftec, como parte dos
            requisitos para avaliação da unidade curricular
            de \disciplina.\par
        }
        \par\vspace{\fill}%
        {\centering%
            \ifdefempty{\orientador}{\relax}{Orientador: \orientador\par\vspace{.65ex}}%
            \ifdefempty{\orientadora}{\relax}{Orientadora: \orientadora\par\vspace{.65ex}}%
            \ifdefempty{\coorientador}{\relax}{Coorientador: \coorientador\par\vspace{.65ex}}%
            \ifdefempty{\coorientadora}{\relax}{Coorientadora: \coorientadora\par\vspace{.65ex}}%
            \par\vspace{\fill}%
            \cidade\par\ano
            \par\vspace{1cm}%
            \cleardoublepage%
        }
    }
}

\ifdefempty{\resumo}{}{\AtBeginDocument{%
    \section*{Resumo}%
    {\small%
        \resumo%
        \ifdefempty{\palavraschave}{\relax}{%
            \par\vspace{.65em}%
            {\bfseries Palavras-Chave: }%
            \palavraschave%
        }%
    }%
    \ifdefempty{\abstract}{\cleardoublepage}{\relax}%
}}

\ifdefempty{\abstract}{}{\AtBeginDocument{%
    \section*{Abstract}%
    {\small%
        \abstract%
        \ifdefempty{\keywords}{\relax}{%
            \par\vspace{.65em}%
            {\bfseries Keywords: }%
            \keywords%
        }%
    }\cleardoublepage%
}}

\AtBeginDocument{%
    \flushbottom%
    \pagestyle{main}%
    \setstretch{1.5}%
    \setlength{\parindent}{15mm}
}
