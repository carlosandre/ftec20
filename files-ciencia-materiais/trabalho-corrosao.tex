\documentclass[capa,palatino,lof,toc]{abnt}

\def\curso{Engenharia} % Civil?
\def\autor{Carlos André Bohn Brandt
      \par Eduardo Martini}

\def\titulo{Demonstração de Problemas relacionados\par à Oxidação no Dia-a-Dia}
\def\subtitulo{Corrosão do Aço no Concreto Armado\par e em Estruturas Metálicas}

\def\disciplina{Ciência dos Materiais}
\def\orientadora{Profª\!\!.\,\@ Ma.\@ Vanessa C. Brambilla}

\makeatletter
\g@addto@macro\normalsize{%
  \setlength\abovedisplayskip{13pt plus 1em}
  \setlength\belowdisplayskip{13pt plus 1em}
  \setlength\abovedisplayshortskip{13pt plus 1em}
  \setlength\belowdisplayshortskip{13pt plus 1em}
}
\makeatother

\usepackage{mhchem}

\begin{document}

\section{Introdução}

Corrosão é a degradação de um material devido à ação química ou eletroquímica
no meio em que este está inserido, e pode estar aliada ou não a esforços
mecânicos. Em geral, é um processo espontâneo, que modifica prejudicialmente as
propriedades dos materiais, reduzindo expressivamente suas propriedades de
resistência com o passar do tempo.

As fontes causadoras da corrosão variam de acordo com o material analisado, e a
forma mais comum deste processo ocorre através do contato do material
desprotegido com gás oxigênio e água em estado líquido. Caso não haja uma
identificação e tratamento adequados, a corrosão pode causar uma oxidação
completa do material, afetando diretamente a estrutura e vida útil de uma
edificação, por exemplo.

Em vista dos problemas que podem ser gerados pela corrosão em materiais com
função estrutural, torna-se importante analisar estas situações, suas causas e
as formas mais eficientes de evitar que este tipo de processo ocorra,
protegendo os materiais envolvidos, e, ainda, de recuperar quaisquer materiais
que possam ter sido deteriorados caso estas medidas de proteção tenham sido
insuficientes.

Deste modo, procura-se analisar duas situações que aparentam ser similares, mas
apresentam causas e indícios completamente diferentes: a aplicação do aço em
estruturas aparentes e em conjunto com o concreto, no concreto armado, sugerindo formas de detectar e evitar estes problemas.

\section{Fundamentação Teórica}

A corrosão é um fenômeno químico, em sua maioria ocasionado por reações de
oxirredução, através das quais ocorre a deterioração de um material através de
uma interação com o ambiente em que este é utilizado ou armazenado
\cite{israel10}, podendo estar associado, também, a esforços mecânicos. De
certa forma, o processo de corrosão é uma forma de dissipação da energia
absorvida e acumulada pelo material em seu processamento, voltando a seu estado
natural em equilíbrio.

Na maioria dos casos, o tipo de material mais associado à ocorrência deste
processo são os metais.  É importante frisar, no entanto, que nem todos os
metais são tão facilmente suscetíveis à corrosão, e, além disso, não só os
metais podem apresentar este fenômeno em condições significativamente mais
agressivas. Além disso, a agressividade de um determinado ambiente para um
material depende do material analisado. Em outras palavras, um ambiente
extremamente agressivo para um material pode ser inofensivo para outro.

Conforme explicam \textcite{santos14}, dentre os materiais que estão sujeitos à
corrosão, o mais frequentemente utilizado é o aço carbono, afirmando que cerca
de 20\% do aço produzido no mundo é destinado à reposição de peças atacadas
pela corrosão. Esta corrosão pode ocorrer em diversas formas, e os agentes
corrosivos mais comuns são a atmosfera, a água, o solo e a exposição a produtos
químicos. A taxa em que esta ocorre pode ser acelerada com um aumento de
temperatura, pressão ou concentração do agente corrosivo, já que estas
condições aumentam a velocidade de difusão.

\subsection{Tipos de Corrosão}

Os tipos de corrosão mais comuns no campo de estudo deste trabalho podem ser
categorizados em corrosão química e corrosão eletroquímica.  Conforme afirma
\textcite{wolynec03}, os processos eletroquímicos de corrosão são os mais
frequentemente encontrados na natureza, e ocorrem através da formação de uma
pilha ou célula de corrosão, na presença de um eletrólito. Na grande maioria
dos casos, este eletrólito é água no estado líquido.

As reações relacionadas à corrosão eletroquímica estão associadas à passagem de
corrente elétrica, com o movimento de íons de uma área da superfície metálica
para outra, e ocorrem à temperatura ambiente.  A precipitação do
produto corrosivo, conhecido como ferrugem, é outra consequência deste
processo, e é basicamente constituído pelos compostos \ce{FeOOH} e \ce{Fe3O4},
obtidos através das reações
\begin{gather}
	\ce{2Fe^{2+}} + \ce{4OH-} + \ce{1/2O2} \rightarrow \ce{2FeOOH} + \ce{H2O}\\
	\ce{8FeOOH} + \ce{2Fe^{2+}} + 2e \rightarrow \ce{3Fe2O4} + \ce{4H2O}.
\end{gather}

Conforme explica \textcite{paredes18}, também pode ser formada uma pilha de
corrosão a partir da interação entre metais diferentes. Neste caso, ocorre uma
transferência de elétrons e íons a partir da diferença de potencial entre ânodo
(onde ocorre predominantemente a oxidação) e cátodo (onde predomina a redução).
O recomendável para evitar este tipo de interação destrutiva é evitar o contato
entre metais com composição diferente, através do uso de materiais isolantes,
ou o uso de inibidores deste tipo de reação química.

Mesmo que os metais façam parte de uma mesma liga, pequenas diferenças em sua
composição ou microestrutura podem desencadear no comportamento de ânodo e
cátodo. Também pode ser formada uma pilha de corrosão caso exista uma diferença
entre a concentração dos eletrólitos, cuja região mais concentrada formará o
cátodo.

Os processos responsáveis pela corrosão podem atingir o material de forma
uniforme, através da formação de pites (pequenas cavidades de profundidade
significativa se comparada à espessura do material) ou a partir do
desprendimento de placas (regiões específicas na superfície do material, em
função de sua composição).

Outro tipo de corrosão que ocorre menos frequentemente, mas, ainda assim, está
presente no cotidiano, é a corrosão química. Também conhecida como corrosão
seca, ocorre na ausência de água no estado líquido, através de uma reação
direta entre o material e o meio, normalmente rico em gás oxigênio (\ce{O2}).
Ocorre, normalmente, em temperaturas acima do ponto de orvalho da água \cite{paredes18}.
A corrosão também pode ocorrer a partir da ação de microorganismos. 

\subsection{Reação de Passivação}

É importante notar a existência de um processo natural que pode auxiliar na
defesa contra a oxidação, através de outra reação na superfície do material, em
decorrência do aumento de pH provocado pela produção de íons \ce{OH-},
produzindo uma película de óxido, conforme exemplificado na equação
\begin{equation}
	\ce{3Fe} + \ce{4H2O} \rightarrow \ce{Fe3O4} + \ce{8H+} + 8e. \label{eqn:passivacao}
\end{equation}

Como explica \textcite{wolynec03}, esta película adere ao metal e é
extremamente fina, protegendo o material da corrosão. Esta película recebe o
nome de película passiva, e reações como a Reação \eqref{eqn:passivacao}
recebem o nome de reações de passivação. Este fenômeno é bastante expressivo na
proteção contra corrosão do alumínio (cuja camada protetora é composta por
\ce{Al2O3}) e do aço inox (cuja camada protetora é composta por
\ce{CrO4^{2-}}), entre outros.

Embora a variação da disponibilidade de \ce{O2} no ar para que estas reações
ocorram seja negligenciável, fornecendo uma quantidade praticamente constante
deste reagente, pode-se verificar que o gás oxigênio apresenta baixa
solubilidade na água. À medida em que a espessura da camada de óxidos aumenta,
é dificultado seu alcance às superfícies expostas do material, desacelerando a
reação, como representado na \autoref{fig:passivacao}.

\figura{passivacao}{Representação do processo de corrosão atmosférica do aço}{silva15}

No entanto, apesar da reação proteger o material sob a película da corrosão,
sua ocorrência não impede a passagem de elétrons, já que sua composição é, em
grande parte, de óxido semicondutor. Em função disso, a passivação não
impede a ocorrência da oxidação em outras regiões do material.

\subsection{Corrosão sob Tensão}

A corrosão sob tensão ocorre quando um material está em contato com meio
corrosivo enquanto submetido a esforços de tração, sejam estes residuais ou
não, e é um dos principais problemas que podem causar a redução de vida útil de
uma estrutura, principalmente se esta já apresentar tensões sem esforços
externos \cite{schroeder99}.  Este processo ocorre em meios especialmente
agressivos, como, por exemplo, soluções contendo cloretos, hidrogênio ou um
carregamento catódico, entre outros. Estas condições não são tão incomuns,
principalmente se for levado em conta o crescente uso de materiais como o
concreto com armaduras protendidas.

Uma das principais características de uma corrosão sob tensão é o rompimento do
tipo frágil, ou seja, que não apresenta estricção. Ocorre, também, a formação
de trincas perpendiculares à direção do esforço, favorecendo a quebra deste
material. Além disso, os produtos da corrosão podem se transformar em novas
fontes de tensão, agravando ainda mais os efeitos deste fenômeno.

\subsection{Prevenção da Corrosão}

De maneira geral, os métodos utilizados para evitar que ocorra a corrosão
envolvem tratamentos superficiais, de modo a evitar que o meio corrosivo entre
em contato com o material vulnerável a este processo. Por exemplo, pode ser
favorecida a formação de uma película passiva na superfície do material, ou a
aplicação de outro material que apresente esta característica. Exemplos desta
prática são os óxidos de crômio (III) e de ferro (III), ou o revestimento por
galvanoplastia, através da qual se reveste o material com outro mais nobre e
menos reativo \cite{fogacaprotecao}.

Além disso, também são frequentemente empregados revestimentos não metálicos,
como o zarcão (uma suspensão oleosa de \ce{Pb3O4}) ou polímeros. Também é
comum a aplicação juntamente com outros metais que apresentem potencial maior
(metal de sacrifício) ou menor (folhas de flandres, por exemplo) de oxidação,
de acordo com as necessidades de uso.

\section{Situações-Problema}

O aço é um dos materiais mais comumente utilizados na Engenharia Civil, em
função de seu excelente relação entre custo e resistência, versatilidade e
durabilidade. Além disso, pode ser empregado em conjunto com outros materiais,
como é o caso do concreto armado, exemplo mais expressivo na construção civil.
Diante desta variedade de formatos e processos, uma das grandes vantagens do
aço neste campo de aplicação, também devem ser observados os processos de
degradação deste material sob pontos de vista distintos e específicos, de modo
a analisar com maior clareza as suas causas e consequências.

\subsection{Corrosão de Estruturas Metálicas}

O uso de estruturas metálicas aparentes representa uma ideia de modernidade e
inovação, não só em função de sua aparência diferenciada, mas também pela
eficiência do uso de mão-de-obra, materiais e tempo de execução, em função da
possibilidade de serem utilizadas estruturas pré-fabricadas.  Além disso,
pode-se adotar um processo construtivo mais industrializado, melhorando a
precisão empregada e reduzindo o desperdício de materiais.

Empregadas, por exemplo, na construção de pontes, complexos industriais, mas
também sendo utilizadas na construção de casas e edifícios residenciais,
diferentes técnicas de construção envolvendo este tipo de material estão em
constante popularização. 

\figura[width=0.8\textwidth]{ironbridge}{Fotografia da {\em Iron Bridge}, construída em 1779 na Inglaterra}{shropshire}

\figura[width=0.8\textwidth]{helixbridge}{Fotografia da {\em Helix Bridge}, construída em 2010 em Singapura}{coxarch}

No entanto, como observado por \textcite{busanello19}, o tempo de construção e
custo da obra podem aumentar significativamente em outras áreas da construção
da edificação.  Neste quesito, é importante identificar a técnica de construção
mais adequada a cada projeto e empregar mão-de-obra habituada com este tipo de
material.

\subsubsection{Ambientes Potencialmente Agressivos}

Os ambientes agressivos com relação à corrosão de uma estrutura metálica
aparente podem variar, de acordo com o local onde ocorre a aplicação desta
estrutura. Por exemplo, quando se trata de pontes sobre rios, a variação do
nível da água pode fazer com que determinada seção da estrutura metálica passe
parte do tempo submersa e outra exposta à atmosfera, estando, desta forma,
exposta a dois ambientes potencialmente corrosivos. Caso a estrutura esteja em
solo rico em cloretos ou sulfatos, estes podem também ser responsáveis pela sua
degradação.

A atmosfera é, em quase todos os casos, uma fonte de corrosão, já que é uma
fonte de oxigênio praticamente inesgotável nesta escala. Além disso, a presença
de água em estado líquido é frequente em locais expostos ao ar livre, em função
da exposição a chuvas e à consensação. Também é através da atmosfera que podem
ser transportados os componentes da chuva ácida. Por conta destes e outros
fatores, a corrosão causada pela atmosfera normalmente é uniforme sobre a
superfície do material. Por outro lado, os agentes corrosivos de elementos
metálicos no solo ou constantemente submersos dependem da composição do meio e
da capacidade de absorção do gás oxigênio. Em diversos casos, a ocorrência da
corrosão nestas condições não é uniforme.

Outra forma de corrosão de estruturas metálicas é a formação de uma pilha
galvânica, resultado da interação direta entre dois ou mais tipos de metais,
onde ocorre uma diferença de potencial elétrico, a partir do qual existe a
transferência de elétrons e íons, conforme exemplificado na
\autoref{fig:galvanica}. A solução para este tipo de problema é um correto
isolamento entre os metais diferentes, como, por exemplo, o uso de fitas
anticorrosivas.

\figura[width=0.7\textwidth]{galvanica}{Exemplo de corrosão pela formação de uma pilha galvânica}{ufpr99}

Deste modo, a análise minuciosa do meio em que a estrutura metálica aparente
está inserida é um dos aspectos que devem necessariamente ser considerados em
projeto, e é essencial que haja uma amenização destes riscos na construção e a
determinação de um planejamento de manutenção durante toda a vida útil da
construção, no qual deve ser feita uma análise detalhada do avanço da corrosão
e da efetividade das medidas tomadas para evitá-la.

\subsubsection{Consequências}

A principal consequência da corrosão nos elementos metálicos é a perda de sua
resistência, em função de sua gradativa diminuição de seção, e da formação de
rachaduras. Em outras palavras, a perda de material pela corrosão e sua
substituição pelo produto corrosivo fazem com que se perda a grande maioria das
propriedades que são desejadas neste tipo de aplicação, como sua resistência
relativamente elevada à tração, por exemplo.

Os defeitos gerados em função deste processo podem ser localizados ou
generalizados, no entanto, caso haja corrosão significativa em um ponto de uma
estrutura, este pode se tornar um ponto frágil, enfraquecendo a capacidade e
resistência de toda a estrutura. Os focos de corrosão podem ter o formato de
placas, alveolar (formato redondo, também conhecido como pítes), intergranular
ou de esfoliação.

\subsection{Corrosão das Armaduras no Concreto Armado}

O concreto é um material compósito, composto de uma mistura de água, cimento e
agregados. É o material mais utilizado na construção civil, apresentando uma
boa resistência à compressão, mas baixa resistência à tração. Em função desta
característica, como ambos os tipos de resistência são desejáveis na maioria
das estruturas, é frequentemente utilizado com outros tipos de materiais.
Destes, o mais comum é o aço, formando, deste modo, o concreto armado. 

A corrosão das armaduras de aço no concreto armado é mais frequente do que
qualquer outro fenômeno de deterioração nas estruturas construídas com este
material, comprometendo-as tanto do ponto de vista estético quanto de sua
segurança. Este fenômeno apresenta causas similares àquelas das estruturas de
aço aparente. No entanto, seus sintomas, consequências e medidas preventivas
são bastante diferentes.

\subsubsection{Causas e Consequências}

O mecanismo de corrosão do aço no concreto, assim como o das estruturas
metálicas aparentes, é eletroquímico, e, tal qual a maioria das reações
corrosivas é em água ou ambiente úmido. Esta corrosão conduz a formação de
óxidos e hidróxidos de ferro, que são produtos de corrosão avermelhados,
pulverulentos e porosos, conhecidos como ferrugem, e só ocorre nas seguintes
condições: devem existir eletrólito, uma diferença de potencial, oxigênio e
agentes agressivos.

\figura[width=0.7\textwidth]{pilar}{Construção em concreto armado atacado pela corrosão}{tecnosil}

Diferentemente das estruturas onde é utilizado somente o aço, a armadura
metálica não é normalmente visível, quando se trata de construções utilizando o
concreto armado. Desta forma, os sinais mais comuns da corrosão das armaduras
do concreto armado são: fissuras e trincas, manchas na superfície do concreto,
desagregações, deformação excessiva, destacamento do concreto, entre
outros.

\figura[width=0.65\textwidth]{descolado}{Exemplo de corrosão em concreto armado}{asopeeng}

O destacamento do concreto se dá pela menor densidade dos produtos da reação,
quando comparados ao seu reagente, o que acaba por gerar uma pressão interna.
Como estes mesmos produtos também apresentam uma aderência reduzida, à medida
em que a reação acontece, torna-se inevitável o destacamento do concreto,
danificando cada vez mais a sustentação da edificação. Além disso, este
destacamento acaba facilitando ainda mais o contato entre a armadura e agentes
corrosivos.

\subsubsection{Mecanismos Naturais de Proteção do Concreto Armado}

O concreto, em suas primeiras idades, é um meio bastante alcalino,
característica esta que protege a armadura da corrosão. No entanto, esta
proteção pode perder sua eficácia caso o concreto, através de um processo
conhecido como carbonatação, perca sua alcalinidade, ou ainda pela ação de
outros agentes, como íons cloreto. Ambos estes processos podem ser evitados
através de uma correta dosagem do concreto, já que esta apresenta menor
permeabilidade.

A carbonatação é um processo que reduz a alcalinidade do concreto, através do
contato dos íons \ce{Ca(OH)2} (liberados no processo de hidratação do cimento)
com o gás carbônico, presente no ar. O mesmo processo pode ocorrer a partir do
contato com, por exemplo, \ce{SO2} e \ce{H2S}. A ocorrência deste processo é
uma das principais condições para o aparecimento da corrosão. Este processo
ocorre incondicionalmente, no entanto é importante que este apresente
progressão lenta o suficiente para que a estrutura não seja atingida durante a
vida útil da obra. A carbonatação pode ser medida a partir da análise de uma
amostra de concreto com indicadores ácido-base, como, por exemplo, a
fenoftaleína.

Além disso, de modo a evitar a ação corrosiva por cloretos, é recomendável
evitar o uso de aditivos aceleradores de pega a base de cloretos, e o uso de
cloreto de sódio como agente anticongelante, já que estes podem causar a
despassivação do aço mesmo em ambientes alcalinos. Em ambientes com atmosfera
que possa conter cloretos (por exemplo, atmosfera marinha), é recomendável um
cuidado especial contra a corrosão, através do uso de aditivos, por exemplo.

Assim como a permeabilidade do concreto utilizado, é essencial um
dimensionamento adequado do cobrimento da estrutura metálica, normatizado
através da \textcite{nbr6118}. Esta norma determina uma classificação dos tipos
de ambientes e determina limites mínimos para este cobrimento no concreto
armado e protendido, este último mais suscetível à corrosão sob tensão.

\subsubsection{Corrosão do Concreto Armado sob Tensão}

Um caso especial da corrosão, que ocorrer mais frequentemente no concreto
protendido, mas também possível no concreto armado convencional, esta classe de
corrosão possui características específicas que a tornam ainda mais perigosa.
Como explicado anteriormente, este tipo de corrosão alia um meio especialmente
agressivo com a aplicação de forças de tração. No caso do concreto armado, seus
produtos acabam sendo invisíveis, o que impede um correto tratamento antes da
ruptura, que apresenta comportamento frágil, ou seja, não apresenta estricção.

\section{Discussão e Análise de Soluções}

De modo a prevenir a corrosão do aço nestas duas diferentes aplicações, foram
sendo criadas e avaliadas diferentes soluções, algumas das quais serão citadas
a seguir. É importante enfatizar que, independentemente das medidas preventivas
determinadas em projeto, é de vital importância um acompanhamento contínuo de
qualquer estrutura que esteja sujeita a grandes cargas, como é o caso dos exemplos
discutidos neste trabalho.

\subsection{Estruturas Metálicas Aparentes}

Uma das alternativas para evitar a corrosão nestes locais é a utilização de
materiais que sejam menos suscetíveis a este tipo de processo químico, como,
por exemplo, alumínio, aço inoxidável, aço patinável ou materiais compósitos de
matriz polimérica reforçada com fibras. No entanto, o emprego destes materiais
apresenta um custo muito elevado, quando comparado ao do aço, e, como o aspecto
econômico é extremamente importante em qualquer construção, acaba por tornar-se
inviável na grande maioria das situações, além de nem sempre atender aos
requisitos de resistência necessários.

A solução mais frequentemente aplicada é a utilização do aço com revestimentos
específicos, que, além de fornecer um aspecto ainda mais diferenciado, impedem
o contato entre aço e oxigênio, diminuindo, assim, a possibilidade de corrosão
sob sua proteção. Exemplos são a pintura, que pode, ainda, receber pigmentos
inibidores de reação especiais (como, por exemplo, o zarcão, citado
anteriormente), e a galvanização, dependendo da agressividade do meio. Estas
soluções podem ser consideradas as mais adequadas, por apresentarem um desempenho
satisfatório a um custo muito menor.

\figura[height=42mm]{feliz}{Fotografia da ponte de ferro de Feliz, inaugurada em 1900}{klein11}

\subsection{Armaduras no Concreto Armado}

A melhor forma de evitar este tipo de problemas no concreto armado é a correta
dosagem dos componentes da massa, de modo que esta seja o mais impermeável
quanto possível, e apresente espessura conforme as normas vigentes. Deste modo,
é evitada a carbonatação e o ataque por cloretos, duas das principais causas da
deterioração deste material.

Além disso, além de evitar adição de materiais potencialmente corrosivos ao
cimento, pode ser recomendado o uso de aditivos como a sílica ativa, que, em
função de seu tamanho reduzido e elevada reatividade pozolânica, torna o
concreto mais denso e impermeável através de reação com produtos da própria
reação de endurecimento do concreto, desta forma impedindo o contato dos
agentes corrosivos com a armadura metálica.

Também poderiam ser utilizados, em casos extremamente específicos, armaduras
compostas por materiais alternativos, mas, comparativamente ao aço, seu alto
custo, baixa disponibilidade e pouca comprovação científica de suas
propriedades (principalmente relacionadas à durabilidade) os tornam um produto
que ainda não apresenta grande utilização no mercado.

\section{Considerações Finais}

Os problemas citados neste trabalho, inicialmente, podem até parecer
semelhantes e apresentar processos químicos em comum, mas, sob uma análise mais
atenta, apresentam características completamente diferentes, e são visualizados
e tratados de maneira distinta.

Ambos ocorrem com frequência na construção civil, e, por conta disso, foram
sendo avaliadas diferentes soluções para cada situação ao longo do tempo. A
partir das descobertas obtidas através destas avaliações, foram sendo
publicadas normas e regulamentações quanto ao dimensionamento das estruturas
e o uso de determinados materiais, levando em conta a projeção de vida útil da
obra projetada e uma classificação de risco adequada.

No entanto, ainda existem casos onde a regulamentação é insuficientemente
específica com relação às suas exigências. Existem outros casos, ainda, que as
normas estão presentes, precisas e corretas, mas não são seguidas. Nestas
situações, é essencial destacar a importância de órgãos públicos fiscalizadores
eficientes, que realmente verifiquem estes requisitos e sua correta aplicação,
preferencialmente antes da aprovação e execução do projeto.

A análise de riscos, não só de corrosão das estruturas, mas de todas as formas
de degradação e diminuição da capacidade de sustentação das edificações são,
impreterivelmente, responsabilidade dos engenheiros, assim como o
acompanhamento durante e após a sua execução, de modo a confirmar a adequação
das definições em projeto e seu comportamento com o passar dos anos.

\addtocategory{hidden}{ufpr99} % imagem, não localizei mais dados sobre a fonte
\nocite{*}
\end{document}
