: title = "Geometria Analítica e Álgebra Linear"
% gladisbortoli@acad.ftec.com.br ou gladisbortoli@gmail.com
: using LaTeXStrings



# Matrizes

O estudo de matrizes faz parte do conteúdo do Ensino Médio, e são conceitos
importantes para o prosseguimento do estudo da Geometria Analítica,
especialmente seus determinantes.

Matrizes são normalmente representadas com letras maiúsculas, e podem ser
construídas a partir de uma regra ou lei de formação.  Os elementos de uma
matriz são representados por uma letra minúscula, correspondente ao nome da
matriz, seguida da linha e coluna da matriz. Por exemplo, o elemento na
primeira linha e segunda coluna de uma matriz $A pode ser representado
por $[a[12]]. 

Um elemento genérico de uma matriz é representado por $[a[ij]] , onde $i é a
linha e $j é a coluna. As dimensões de uma matriz, também conhecidas como
sua _ordem_, são normalmente representados pelas letras $m (linhas) e $n
(colunas).

$ A = par(a[ij], sub="2X2") &" em que "& a[ij] == 3i-j

$ A = [ 2 1; 5 4 ]

Pode-se representar uma matriz entre parêntesis, colchetes ou chaves. Os
elementos de uma matriz $A são indicados da seguinte forma:

$ A = [ a[11] a[12] a[13] a[14] L"\dots"
		a[21] a[22] a[23] a[24] L"\dots"
		a[31] a[32] a[33] a[34] L"\dots"
		L"\vdots" L"\vdots" L"\vdots" L"\vdots" ]



## Determinante

O determinante de uma matriz é um valor que possui diversas utilizações na
Geometria Analítica. Normalmente, é representada pela representação da matriz
entre linhas:

$ det([ a[11] a[12]; a[21] a[22] ]) &" ou "& det(A)

A resolução do determinante de uma matriz difere, dependendo da ordem da
matriz. Em outras palavras, cada ordem de matriz possui sua forma diferente de
resolver. Para que seja possível calcular o determinante de uma matriz, é
necessário que esta tenha o número de linhas e colunas igual, ou seja, que a
matriz seja quadrada.

O valor do determinante de uma matriz 2x2 é obtido através da multiplicação
entre os valores da diagonal principal, subtraindo a multiplicação da diagonal
secundária.

!img det2x2.png

O processo para descobrir o determinante de uma matriz 3X3 é conhecido como
Regra de Sarrus. A matriz é copiada, e, à direita, repete-se as duas primeiras
colunas. A partir disto, tem-se três diagonais principais e três secundárias.
Cada uma das diagonais tem seus componentes multiplicados entre si, somando as
diagonais principais e subtraindo as secundárias, como no exemplo:

!img sarrus.png



# Sistema Cartesiano Ortogonal

É um sistema de representação em um plano criado pelo matemático francês René
Descartes. Do produto cartesiano entre dois conjuntos $A e $B resulta outro
conjunto, cujos elementos são formados por duas partes, oriundas dos dois
conjuntos originais. Esta entidade matemática recebe o nome de *par ordenado*.
De modo a representar o conjunto $[times(A, B)], são utilizadas duas retas reais,
dispostas de forma perpendicular, que recebem o nome de eixos coordenados.  Ao
fazer o produto cartesiano do conjunto dos números reais consigo mesmo, tem-se
o plano cartesiano, conhecido também como $[A^2].

Com a interseção dos eixos, observa-se a formação de quatro quadrantes:

!img quadrantes.png


## Ponto

Pode-se definir o ponto como o resultado do produto cartesiano entre dois
conjuntos, ou como um elemento de produto cartesiano entre dois conjuntos não
vazios. Em outras palavras, pode ser entendido como o endereço de uma certa
posição em um plano.

Para cada ponto $P, está associado um par ordenado, representado da
forma $[(x[p], y[p])]. A coordenada $x é denominada abscissa, e a
coordenada $y, ordenada.


## Distância entre dois Pontos

O comprimento de um segmento de reta é obtido através do Teorema de Pitágoras.
Para um segmento qualquer entre os pontos $[(x[1], y[1])] e $[(x[2], y[2])],
tem-se:

$ d_AB^2 = (x[2] - x[1])^2 + (y[2] - y[1])^2
$ d_AB = sqrt((x[2] - x[1])^2 + (y[2] - y[1])^2)


## Ponto Médio de Segmento de Reta

De modo a determinar as coordenadas de um ponto médio $M de um segmento de
reta, determina-se as médias entre as coordenadas das extremidades do segmento:

$ x_M = (x[1] + x[2]) / 2
$ y_M = (y[1] + y[2]) / 2


## Cálculo de Área através do Determinante

Pode-se calcular a área de um triângulo no plano cartesiano através de uma
matriz contendo as coordenadas de seus pontos. Esta matriz é construída de modo
que a primeira coluna contenha os valores de $x, e a segunda coluna contenha os
valores $y. A terceira coluna é preenchida com o valor 1. A área do triângulo
corresponde à metade do valor absoluto do determinante desta matriz:

$ M = [ x[1] y[1] 1
$       x[2] y[2] 1
$       x[3] y[3] 1 ]
$ "Área" = (1/2) * abs(det(M))


## Mediana

$ x[G] = (x[A] + x[B] + x[C]) / 3

Define-se como mediana o segmento de reta que parte de um ângulo de um
triângulo e atinge o ponto médio do segmento oposto. Por exemplo, na imagem a
seguir está ilustrada a mediana relativa ao ângulo $[ang(B)].

!img mediana.png

O ponto de encontro das três medianas de um triângulo é determinado
*baricentro* (indicado pela letra $G), que o corta em uma razão de 2:1. As
equações para determinação do baricentro de um triângulo $[A&B&C] são:

$ x[G] = (x[A] + x[B] + x[C]) / 3
$ y[G] = (y[A] + y[B] + y[C]) / 3

## Alinhamento entre Três Pontos

Dadas as coordenadas de três pontos quaisquer no plano cartesiano, pode-se
verificar se estão alinhados, ou seja, se fazem parte da mesma reta: para todos
conjuntos de pontos em alinhamento, o determinante formado por seus pares
ordenados deve ser igual a 0. Caso o determinante não seja 0, formar-se-á um
triângulo, cuja área equivale a metade do determinante.

$ det([x[1] y[1] 1; x[2] y[2] 1; x[3] y[3] 1]) == 0



# Equação de uma Reta

Pode-se definir a reta, no contexto da Geometria Analítica, como uma sucessão
de infinitos pontos, distintos e alinhados em uma direção constante. Uma reta
não tem fim, e divide o plano em duas partes. A equação de uma reta qualquer
pode ser determinada a partir de dois de seus pontos, já que só existe uma
única reta que passe por estes dois pontos.

A equação da reta pode ser determinada a partir do cálculo do determinante:

$ det([x y 1; x[A] y[A] 1; x[B] y[B] 1]) == 0

Simplificando,

$ (y[A] - y[B])*x + (x[B] - x[A])*y + (x[A]*y[B] - x[B]*y[A]) == 0

Desta forma, a *equação geral da reta* assume a forma:

$ a*x + b*y + c == 0

Algumas literaturas sugerem que a constante $a seja positiva, se necessário,
multiplicando toda a equação por $[-1].

A partir da equação geral, também pode-se isolar o termo $y, obtendo a *equação
reduzida da reta*:

$ y == m*x + n
m: coeficiente angular
n: coeficiente linear

O valor $n representa o local onde a reta corta o eixo $y, já que, ao
substituirmos $x por $0 na equação reduzida, tem-se $[y==n]. Tanto $n quanto $m
devem obrigatoriamente ser extraídos da equação reduzida.

O coeficiente angular $m representa a inclinação da reta. Um coeficiente
positivo representa uma função crescente, enquanto que negativo indica uma
função decrescente. Pode-se dizer, por exemplo, que duas retas são *paralelas*
quando seus coeficientes angulares são iguais, e *concorrentes* quando são
diferentes. Os coeficientes angulares de retas *perpendiculares* apresentam a
seguinte relação:

$ m[r] == -1/m[s]

Existem 3 maneiras de descobrir o coeficiente angular, utilizadas de acordo com
a disponibilidade de dados sobre a reta analisada:

- Equação Reduzida da Reta ($[y==m*x+n])
- Tangente do Ângulo ($[m==tan(alpha)])
- Fórmula utilizando Dois Pontos ($[m == (y_2-y_1)/(x_2-x_1)])



# Circunferência

Representa o conjunto de todos os pontos de um plano com uma distância fixa de
um determinado ponto. Esta distância é denominada *raio*. A equação reduzida de
uma circunferência com raio $r e centro no ponto $[(x[0], y[0])], a partir do
Teorema de Pitágoras, é:

$ (x-x[0])^2 + (y-y[0])^2 == r^2

Expandindo a equação reduzida, obtém-se a equação geral da circunferência:

$ x^2 + y^2 - 2x[0]*x - 2y[0]*y + x[0]^2 + y[0]^2 - r^2 = 0

Quando tem-se a equação geral de uma reta, também pode-se extrair os dados da
circunferência (centro e raio) a partir das seguintes equações:

$ x^2 + y^2 + alpha*x + beta*y + gamma
$ C = (-alpha/2, -beta/2)
$ r = sqrt(x[0]^2 + y[0]^2 - gamma)

# Cônicas

Uma seção cônica, também conhecida somente por cônica, é uma curva obtida
obtida através do corte de qualquer cone de duas folhas por um plano que não
passa pelo vértice (também conhecido como plano secante). Estas intersecções
podem ser um círculo, parábola, hipérbole ou elipse.

!img conicas.png

## Parábola

Uma parábola é formada pelo conjunto dos pontos na intersecção entre um cone
duplo e um plano paralelo à sua geratriz. Na geometria plana, é formada pelos
pontos equidistantes entre um ponto fixo $F, denominado foco, e uma reta
fixa $D do plano. Ela pode ser classificada de acordo com seu eixo de simetria,
em $x ou $y.

A equação de uma parábola com simetria no eixo $x, vértice na origem e foco no
ponto $[(p, 0)] é:

$ y^2 = 4*p*x

Caso o eixo de simetria seja $y e o foco seja $[0, p], a equação é:

$ x^2 = 4*p*y

!img parabola.png

## Elipse

Uma elipse é composta do conjunto de pontos na intersecção entre um cone em
apenas uma de suas folhas, e um plano não paralelo a sua base. Na geometria
plana, é o conjunto de pontos $P do plano em que a soma das distâncias a dois
pontos fixos $[F_1] e $[F_2], denominados Focos da elipse, é constante
($[d_1 + d_2 = "constante"]). A equação reduzida da elipse com foco no eixo $y
e centro na origem é:

$ x^2 / a^2 + y^2 / b^2 = 1

Neste caso, o semieixo maior equivale a $[2*a], o menor é $[2*b] e a distância
focal $[2*c]. No caso de o eixo maior ser o $y, troca-se os valores de $a e $b.
Em outras palavras, o denominador $[a^2] sempre estará acompanhando o símbolo
cujo eixo onde a elipse é maior.  Em ambos os casos, determina-se como
excentricidade da elipse o seu coeficiente de "achatamento", determinado por:

$ e = c/a

Tendo dois destes valores ($a, $b, $c ou $e), pode-se utilizar o Teorema de
Pitágoras para identificar o faltante:

$ a^2 = b^2 + c^2

## Hipérbole

Uma hipérbole é composta pelos pontos cuja diferença entre as distâncias a dois
pontos fixos (denominados focos) é uma constante positiva e menor que a
distância entre estes dois pontos. Denomina-se *eixo real* da hipérbole o eixo
onde os focos estão localizados. A equação de uma hipérbole cujo eixo real é o
x é:

$ x^2/a^2 - y^2/b^2 = 1

A equação de uma hipérbole com focos no eixo y é:

$ y^2/a^2 - x^2/b^2 = 1

Em outras palavras, o eixo real sempre está relacionado à fração positiva e ao
valor $a. A distância entre cada um dos focos e a origem é $c, a distância
entre os vértices $[V[1]] e $[V[2]] (que se localizam no eixo real da
hipérbole) e a origem é $a. No eixo imaginário, localizam-se os pontos $[M[1]]
e $[M[2]], distantes $b unidades da origem.

Tendo os pontos $[M[1]], $[M[2]], $[V[1]] e $[V[2]] como pontos médios de suas
arestas, pode ser formado um retângulo, cujas diagonais fazem parte das
assíntotas da hipérbole ($[r[1]] e $[r[2]]). Assim como as elipses, as
hipérboles também apresentam excentricidade, também descrita por:

$ e = c/a

Além disso, também pode-se determinar uma relação com o Teorema de Pitágoras:

$ c^2 == a^2 + b^2

As assíntotas apresentam equações:

$ y = (-b/a) * x
$ y =  (b/a) * x

# Equações das Cônicas com Centro Genérico

As equações apresentadas anteriormente apresentam centro ou vértice
em $[(0,0)]. agora, analisaremos as equações com centro ou vértice em um ponto
genérico, representado como $[(h, k)], cujos focos estão na reta $[y=k] ou
em $[x=h]. Em outras palavras, os focos continuam paralelos aos eixos $x e $y.

Em todos estes casos, substitui-se os símbolos por subtrações, de modo a mudar a
posição das cônicas no plano: substitui-se $x por $[par(x-h)], e $y
por $[par(y-k)]. Na parábola, o vértice é $[(h, k)], e na elipse e hipérbole, o
centro é $[(h, k)].


# Cálculo Vetorial

Um vetor é, em síntese, um segmento de reta orientado. Ele possui um
módulo (comprimento), direção e sentido. Vetores com mesmo módulo, direção
e sentido são denominados equipolantes. Um vetor $v pode ser representado
como $[vec(v)]. 

## Soma e Subtração de Vetores

Graficamente, pode-se expressar a soma de vetores como a colocação do segundo
vetor na extremidade do primeiro. O vetor entre o início do primeiro e o final
do segundo representa a soma entre eles. Caso não haja necessidade ou
possibilidade de representar graficamente os vetores, sua soma será a soma
de seus componentes, por exemplo, pode-se determinar o valor
de $[vec(x) = vec(v) + vec(a)]:

$ vec(v) = (0, 3)
$ vec(a) = (5, 0)
$ vec(x) = par((0+5), (3+0))
$ vec(x) = par(5, 3)



## Produto Escalar

O Produto Escalar é uma operação realizada entre vetores, que tem como
resultado um valor escalar. Também é conhecido como produto interno.
Representa-se o produto escalar como $[dot(vec(u), vec(v))], e pode ser
calculado entre vetores de dois, três ou mais componentes, utilizando o mesmo
processo. Pode-se obter o seu valor a partir da soma dos produtos dos
componentes de cada vetor:

$ dot((a_1, b_1), (a_2, b_2)) = a_1 * a_2 + b_1 * b_2

Por exemplo, seja $[vec(u) == (-1,2)] e $[vec(v) == (2, 3)]:

$ (-1) * 2 + 2 * 3 == 4

### Ângulo entre dois vetores

A principal aplicação do produto escalar é o ângulo entre vetores:

$ cos(theta) == dot(vec(u), vec(v)) / dot(abs(vec(u)), abs(vec(v)))
$ par(abs(vec(x)) == sqrt(a^2 + b^2))

A partir desses conhecimentos, também pode-se determinar a condição de
paralelismo entre dois vetores, já que não existe ângulo entre eles.

$ parallel(vec(u), vec(v)) => x1/x2 == y1/y2

### Vetores Ortogonais

Dois vetores são ortogonais (representação $[perp(u, v)]) quando o ângulo formado
por eles for equivalente a 90 graus. Pode-se determinar esta característica
quando seu produto escalar for igual a 0.  Para que sejam considerados
ortogonais não é necessário que os vetores se encostem, apenas é necessário que
a interseção entre suas extensões apresente um ângulo reto.

### Projeção de um Vetor

Representa a projeção ortogonal de um vetor sobre uma linha reta paralela a
outro.  A projeção de vetores é representada como $[vec(w) == proj(vec(v),
vec(u))].  Pode-se determinar a projeção do vetor $[vec(u)] sobre o
vetor $[vec(v)] com o uso da seguinte equação:

$ proj(vec(v), vec(u)) == dot(par(dot(vec(u),vec(v)) / dot(vec(v),vec(v))), vec(v))

!img projecao.png

### Vetores unitários

Em um sistema cartesiano ortogonal, de modo a representar vetores no
plano $[bb(R)^2] podemos utilizar como base os vetores cujas origens são a
origem do plano cartesiano e extremidades os pontos $[(1,0)] e $[(0,1)],
respectivamente conhecidos como $[vec(i)] e $[vec(j)]. Em outras palavras, o
vetor $[vec(i)] é paralelo ao eixo x, enquanto que $[vec(j)] é paralelo ao eixo
y. Quando se analisa em três dimensões, $[vec(k)] é paralelo ao eixo z.

Qualquer vetor no plano cartesiano pode ser decomposto segundo as direções 
de $[vec(i)] e $[vec(j)], de modo que a este seja igual a soma de dois
vetores $[a*vec(i) + b*vec(j)]. Um vetor bidimensional pode ser representado
genericamente pelo par ordenado $[(a, b)].



## Produto Vetorial

É um produto entre dois vetores, cujo resultado é um vetor. É um recurso da
álgebra linear, que pode ser utilizado em diversas áreas da física, por
exemplo. Considerando-se vetores pertencentes ao espaço $[bb(R)^3], define-se o
produto vetorial $[times(vec(u), vec(v))] (ou $[vec(u) && vec(v)]) como um
vetor cujo:

- Módulo é dado por $[vec(w) == abs(vec(u)) * abs(vec(v)) * sin(theta)]
- Direção é perpendicular ao plano dos vetores $[vec(u)] e $[vec(v)]
- Sentido é dado pela regra da mão esquerda.

O produto vetorial não está definido em $[bb(R)^2]

Também pode-se calcular o produto vetorial através do determinante da matriz:

$ ( (x_1, y_1, z_1) && (x_2, y_2, z_2) ) == %
  det([ vec(i) vec(j) vec(k); x_1 y_1 z_1; x_2 y_2 z_2 ])

Além disso, o módulo do produto vetorial é igual à área de um paralelogramo
cujos lados compreendem os dois vetores, e metade do produto é a área de um
triângulo formado por eles. O produto vetorial apresenta as seguintes
propriedades:

- Anti-comutativa: $[vec(u) && vec(v) == -vec(v) && vec(u)]
- Associativa: $[k * par(vec(u) && vec(v)) == par(k*vec(u)) && vec(v) %
									       == vec(u) && par(k*vec(v))]
- Distributiva em relação à adição de
  vetores: $[vec(u) && par(vec(v) + vec(w)) == %
             vec(u) && vec(v) + vec(u) && vec(w)]


## Produto Misto

A principal aplicação do produto misto é o cálculo de volumes cujos vértices
são vetores. O produto misto é calculado da seguinte forma:

$ par(vec(u), vec(v), vec(w)) == dot(par((vec(u) && vec(v))), vec(w))

Também pode-se calcular o determinante da matriz:

$ par(vec(u), vec(v), vec(w)) == det([x[1] y[1] z[1]; %
									  x[2] y[2] z[2]; %
									  x[3] y[3] z[3]])

Para o volume de um tetraedro, deve-se dividir o valor por 6:

$ par(vec(u), vec(v), vec(w)) == (1/6) * dot(par((vec(u) && vec(v))), vec(w))

O sinal do produto misto varia de acordo com o ângulo formado entre o 
vetor $[vec(w)] e o resultado de $[vec(u) && vec(v)]. Caso este ângulo seja
agudo, o sinal é positivo, caso seja obtuso, é negativo. 

### Propriedades

- Cíclico: a permuta circular dos fatores não altera o produto misto. Quando se
  permuta de forma não cíclica, o produto misto somente troca de sinal.
- Permuta dos Sinais: o produto misto não se altera quando se permuta as operações.


## Produto Duplo

Chamam-se produto duplo vetorial ou duplo produto externo o resultado das
seguintes equações:

$ par( vec(u) && vec(v) ) && vec(w)
$ vec(u) && par( vec(v) && vec(w) )

Estes dois vetores *são distintos*, e é imprescindível o uso de parênteses.
