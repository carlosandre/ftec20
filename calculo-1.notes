: title="Cálculo I"

% judotto45@gmail.com

# Funções

Função é uma lei que está associada a uma determinada situação. Por exemplo,
pode se associar o salário $S de um vendedor sobre o valor $x em vendas
efetuadas no mês. Considerando que este vendedor recebe R$ 1.000,00 mensais,
mais 10\% sobre o valor vendido em produtos, a função que representa esta
situação é:

$ S(x) == 1000 + 0.1*x

Os tipos mais comuns de funções, estudadas nas disciplinas de Cálculo, são:

- Função de 1º Grau: $[f(x) = a*x + b]
- Função de 2º Grau: $[f(x) = a*x^2 + b*x + c]
- Função Exponencial: $[f(x) = a^x]
- Função Logarítmica: $[f(x) = log(a, x)]
- Função Trigonométrica: $[f(x) = sin(x)]

Nesta disciplina, serão estudadas normalmente as funções de primeiro e segundo
grau.

## Função Composta

Utilizada frequentemente no estudo das derivadas, resulta da "mistura" de
diferentes funções. A notação para a função composta é:

$ f(g(x)) & " ou " & (f ∘ g(x))

# Limites

O Limite de uma função é uma forma de descrever o seu comportamento à medida
que sua variável independente atinge um valor determinado.  No estudo dos
limites, o sentido em que esta aproximação acontece pode variar, e é indicado
através de um sinal positivo ou negativo.

O limite da função $[f(x)], no valor 3 e aproximando-se a partir da direita (ou
seja, de valores maiores do que 3), pode ser representado como:

$ lim(f(x), x=>3^:+)

Por outro lado, o limite a partir da esquerda (valores menores que 5, por
exemplo), é representada por:

$ lim(f(x), x=>5^:-)

Os limites em que a direção é considerada são denominados *limites laterais*.
Nos casos em que ambos os sentidos devolvem o mesmo limite, não é necessário
indicar o sinal. Caso os resultados entre os sentidos sejam diferentes, não
existe um limite sem um sinal explicitamente indicado.

Os limites podem ser representados graficamente, por exemplo, na função:

$ f(x) = if x < 3
$ 	-1
$ elseif x == 3
$ 	1
$ elseif x > 3
$ 	3
$ end

!image grafico-limites.png

$ lim(f(x), x=>3^:-) == -1
$ lim(f(x), x=>3^:+) == 3
$ lim(f(x), x=>3) == "não existe"

O valor da função no ponto $[x==3] não é considerado na determinação do limite,
apenas os valores da função à medida em que o valor de $x se aproxima de 3.

Ainda existem casos onde a função não apresenta nenhum limite aparente,
continuando a crescer ou decrescer infinitamente. Nestes casos, diz-se que os
termos da sucessão tendem para o infinito, ou, ainda, que o limite desta
sucessão é infinito.

Muitas vezes, o cálculo do limite de uma função não será tão simples. Isso se
deve ao fato de que, frequentemente, a ação de inserir os valores na função
trará uma indeterminação matemática. Exemplos de indeterminações são:

$ 0 / 0, x/0, ∞/∞, ∞-∞, 0^0

Ao encontrar uma destas formas, ou outras formas de indeterminação, deve-se
utilizar técnicas que permitam encontrar expressões equivalentes à forma
inicial, mas que não representam indeterminações. O caso mais comum de
indeterminação é $[0/0].

Dentre estes artifícios, o mais utilizado e eficaz é a fatoração das equações.
As formas mais comum de fatoração utilizadas no estudo dos limites são:

$ (a^2 - b^2)     == (a+b)*(a-b)
$ (a+b)^2         == a^2 + 2*a*b + b^2
$ (a-b)^2         == a^2 - 2*a*b + b^2
$ a^2 + 2*a       == a*(a+2)
$ a*x^2 + b*x + c == a*(x-x_1)*(x-x_2)
$ (a^3 + b^3)     == (a+b)*(a^2 - (a*b) + b^2)
$ (a^3 - b^3)     == (a-b)*(a^2 + a*b + b^2)

!image prop-limites1.png
!image prop-limites2.png

# Limites Infinitos

Ocorrem quando as imagens da função ficam continuamente maiores à medida em que
se aproxima do limite, superando qualquer valor que poderia ser fixado. No
exemplo a seguir, o denominador fica continuamente mais próximo a zero, e,
desta forma, o valor da imagem cresce continuamente:

$ lim(5/(x-3), x=>3^:+) = ∞
$ lim(5/(x-3), x=>3^:-) = -∞

Da mesma forma, pode-se estudar o comportamento de uma função à medida em que o
valor de $x se aproxima do infinito positivo ou negativo, onde está
caracterizado um *limite ao infinito*. Por exemplo, quando o denominador de uma
fração é infinito, o quociente tende a zero.

$ lim(1/x, x=>_±∞) = 0

Quando o denominador de uma fração tende a zero,

$ lim(1/x^n, x=>0^:+) == _+∞
$ lim(-1/x^n, x=>0^:+) == _-∞
$ lim(1/x^n, x=>0^:-) == if iseven(n)
$	_+∞
$ elseif isodd(n)
$	_-∞
$ end
$ lim(-1/x^n, x=>0^:-) == if isodd(n)
$	_-∞
$ elseif iseven(n)
$	_+∞
$ end

No caso de limites de polinômios, em que $x tende aos extremos ($[∞] ou $[-∞]),
pode-se considerar apenas o termo cujo expoente for o maior, já que o polinômio
pode ser decomposto, e, ao colocar o maior expoente em evidência, os outros
termos tendem a 0. Por exemplo:

$ lim(2x^3 + 4x^2 - 5x + 9, x=>∞) == lim(2x^3 * (1+2/x-5/2x^2 + 9/2x^3), x=>∞)
$ _== lim(2x^3, x=>∞) == ∞

Existem funções cujos limites nos extremos não existem. É o caso de funções
trigonométricas, já que estas são periódicas. Também existem alguns casos de
limites especiais, que não ficam imediatamente óbvios na substituição de
valores:

Limite Exponencial Fundamental:
$ lim((1+1/x)^x, x => _+∞ &" ou "& _-∞) == exp(1)

Limite Trigonométrico Fundamental:
$ lim(sin(x)/x, x=>0) == 1

# Continuidade de uma Função

Informalmente, considera-se como uma função contínua aquela cujo gráfico não
apresenta interrupções. Uma função pode ser contínua apenas em um determinado
intervalo, ou em todo o seu domínio. Se uma função não é contínua em um
ponto $a, dizemos que ela é descontínua nesse ponto. Ao analisar a continuidade
de uma função em um ponto $a, pode-se observar a relação, desde que $f seja
definida em $a:

$ lim(f(x), x=>a)&" existe e "&lim(f(x),x=>a) == f(a)

Por exemplo, funções polinomiais são contínuas em todo o seu domínio.

# Função Derivada
 
Em termos gerais, a derivada representa a taxa de variação instantânea de uma
função. Desta forma, pode-se determinar a derivada de uma função $[f(x)],
definida em um intervalo aberto contendo $x, a partir do limite

$ f′(x) == lim((f(x+Δx) - f(x)) / Δx, Δx => 0)

Outro conceito intimamente ligado ao estudo de derivada é a determinação da
reta tangente à função estudada em um determinado ponto $[(x, y)]. De modo de
observar graficamente a derivada de uma função, corta-se uma função $[f(x)] por
uma reta secante $s, a qual cruza a função nos pontos $P e $Q. A medida em que
se aproxima estes pontos, de modo que sua distância tenda a 0, aproxima-se de
uma reta $t, tangente à função.

% https://www.geogebra.org/graphing/ahp7skzy
!img tangente.gif

Em outras palavras, à medida em que Q se movimenta, de modo que $[Δx => 0], a
reta secante $s tende à reta tangente $t.  Desta forma, como só é conhecido um
ponto da reta $t (já que ela só encontra a curva em um único ponto), o
coeficiente angular ($m) da reta tangente pode ser expressa em função de um
limite, conforme a equação

$ m_tangente == f′(x) == Δy/Δx == tan(α)

A partir de um ponto de vista físico, a derivada representa a taxa de variação
instantânea de uma função em um determinado ponto; já partindo de uma
interpretação geométrica, representa o coeficiente angular da reta tangente à
curva em função da coordenada $x do ponto de tangência.

## Regras de Derivação

### Derivada de uma Constante

Se $c é uma constante e $[f(x)==c] para todo $x,
$ f′(x) == 0

### Regra da Potência

Se $n é um número inteiro positivo, e $[f(x) == x^n],
$ f′(x) == n * x^(n-1)

### Derivada do Produto de uma Constante por uma Função
Se $c é uma constante e $[y == c * f(x)],

$ y′ == c * f′(x)

Por exemplo, se $[y == c*x^n], 
$ y′ = c*n*x^(n-1)

### Derivada de uma Soma ou Subração

A derivada da soma ou subtração equivale à soma/subtração das derivadas.

$ y == f(x) + g(x)
$ y′ == f′(x) + g′(x)

### Derivada de um Produto

Sejam $f e $g duas funções e $h a função definida por $[y == f(x) * g(x)],
$ y′ == f(x) * g′(x) + f′(x) * g(x)

### Derivada de um Quociente
Sejam $f e $g duas funções e $h a função definida por $[y == f(x) / g(x)],
$ y′ == ( f′(x) * g(x) - f(x) * g′(x) ) / (g(x))^2

### Regra da Cadeia
Utilizada em funções compostas. Sejam $f e $g duas funções,
e $[y == f(g(x))],

$ y′ == f′(g(x)) * g′(x)

### Derivada de Potência de Base Composta
Este é um caso específico da Regra da Cadeia. Seja $[y=(f(x))^n], com um $n inteiro e positivo,
$ y′ == n*(f(x))^(n-1) * f′(x)



## Aplicações da Derivada

Uma das situações em que a derivada se apresenta mais naturalmente é na
determinação de uma velocidade instantânea, já que esta pode ser aproximada por
uma velocidade média, e, gradativamente, pode-se aproximar do resultado real a
partir de um limite. Na disciplina de Física, determina-se que a velocidade
média é:

$ V_média = ΔS / Δt

De modo a determinar a velocidade instantânea de uma partícula, vai se
diminuindo o intervalo de tempo estudado, de modo que $[Δt => 0]. Desta forma,
a velocidade instantânea pode ser descrita como:

$ V_inst = lim(ΔS/Δt, Δt => 0)
$ V_inst = lim((S(t+Δt) - S(t))/Δt, Δt => 0)

Esta expressão é exatamente igual à derivada de uma função, e, portanto,
pode-se definir que a velocidade instantânea é igual à derivada da distância:

$ v(t) = S′(t)

Da mesma forma, pode-se estudar a variação da velocidade, denominada
aceleração. A derivada da velocidade nos fornece a aceleração instantânea em
qualquer instante $t.

$ a(t) = v′(t)

Outra aplicação das derivadas é a análise marginal, que representa uma
aproximação do custo de produto de uma unidade adicional de um determinado
produto. Em outras palavras, o custo marginal é a taxa de variação instantânea
da função, aproximando-se da variação entre um valor $q e $[q+1].

$ C(q)  = "custo total"
$ C′(q) = "custo marginal (derivada do custo total)"
$ C′(q) ≈ "custo de produção da unidade "& (q+1)

Em outras palavras, a estimativa do custo de produção da $[n]-ésima unidade de um
determinado produto é determinada por $[C′(n-1)].

Além disso, pode-se fazer análises acerca dos extremos absolutos de uma função
em um intervalo utilizando estes conceitos. De modo a encontrar estes pontos,
encontra-se os pontos críticos da função, que são os pontos em que sua derivada
é igual a 0. Estes pontos são os valores em que a curva muda de direção.

!img pontos-criticos.png

De modo a determinar estes pontos críticos, deriva-se a função e calcula-se
suas raízes. Os valores de $x para os quais a derivada da função equivaler a
zero representam os pontos críticos da função analisada. Nesta análise, o
mínimo e máximo absolutos podem ser determinados comparando as imagens da
função em seus pontos críticos e nos extremos do intervalo estudado.

!img minmax-derivada.png

# Integral Indefinida

Muitos problemas da geometria e da física dependem de um processo inverso à
derivação, conhecido como antiderivação ou antidiferenciação. Dada a derivada
de uma função, encontra-se a função original. Por exemplo, tendo a taxa de
crescimento de uma população (expressa por uma função), pode-se prever o
tamanho da população em algum instante futuro.

Uma função $[F(x)] é a função primitiva de $[f(x)]. Esta primitiva não é única,
já que, por exemplo a função

$ f(x) = x^2 + 5

poderia ter como primitivas as funções $[F(x) = (x^3)/3 + 5x + 5], $[G(x) = (x^3)/3 + 5x - 1] 
ou $[H(x) = (x^3)/3 + 5x + c], onde $c é uma constante qualquer. Desta forma,
pode-se concluir que a solução da equação acima deve ter a forma a seguir para
alguma constante $c, conhecida como constante de integração:

$ (x^3)/3 + 5x + c

Desta forma, se $[F(x)] é uma função primitiva de $[f(x)], a
expressão $[F(x)+c] é a integral indefinida da função $[f(x)], denotada por:

$ int(f(x), x) == F(x) + c

No caso de polinômios, os expoentes são acrescidos, e divide-se os termos pelo
valor final do expoente. Por exemplo:

$ x^3 => x^4/4

Além disso, diversas outras propriedades se aplicam, como, por exemplo:

$ int(k*f(x), x) == k*int(f(x), x)
$ int(f(x)+g(x), x) == int(f(x), x) + int(g(x), x)
$ int(f(x)-g(x), x) == int(f(x), x) - int(g(x), x)

$ int(cos(x), x) == sin(x) + c
$ int(sin(x), x) == -cos(x) + c

# Integral Definida

A integral definida é uma das aplicações da integral indefinida, na qual se
determina, a partir de uma função derivada, a diferença entre os valores de
dois pontos em sua primitiva. Além disso, a integral definida também pode
determinar a área entre o gráfico de uma função, o eixo das abscissas e duas
linhas verticais nos valores em que o valor $x equivale a $a e $b,
respectivamente. 

Este processo é definido através do *Teorema Fundamental do Cálculo:*

$ int(f(x), x, a, b) = F(b) - F(a)

onde $[F(x)] é a função primitiva de $[f(x)]. Também pode-se representar o
mesmo conceito de outra forma:

$ int(g′(x), x, a, b) = g(b) - g(a)

## Teorema do Valor Médio

Dentro de um certo intervalo de uma função $f contínua, sempre existe um ponto,
não necessariamente na metade deste intervalo, onde se tem o valor médio.

