: title = "Gestão Ambiental"

# Introdução à Gestão Ambiental

A Gestão Ambiental é a disciplina que estuda a interação entre homem e
ambiente. Um dos objetivos da introdução à gestão ambiental é melhorar a
compreensão e identificação e apresentar métodos de melhorar diferentes
situações adversas ao ambiente.

Um grande exemplo do cotidiano em que existem diversas contradições é a
destinação de rejeitos para reciclagem. Por exemplo, os seguintes materiais são
recicláveis, mas não existem processos economicamente viáveis para sua extração
e reutilização dos materiais:

- Sacolas Plásticas (o produto não reciclado é muito barato)
- Embalagem de Salgadinho (não existe processo para separação dos diferentes
  materiais)
- Caixa de Leite (Tetra-Pak) (o processo existe, mas é muito caro)

Fenômenos como a poluição são eventos que alteram negativamente a forma natural
do ambiente.  Em contrapartida, nem todas as alterações do ambiente feitas pelo
ser humano são maléficas ao ambiente, como por exemplo o reflorestamento.  Por
conta disso, é importante o estudo acerca dos impactos de uma ação, que,
dependendo da atividade, é obrigatório a qualquer empreendimento.

Quando se utiliza algum recurso natural em qualquer processo, por exemplo, a
água, o resultante desta ação nunca é devolvido da mesma forma em que foi
retirada do ambiente. Por conta disso, também é importante determinar padrões
de qualidade da água, ar, solo, entre outros, de modo que sejam determinados
limites aceitáveis dentro dos parâmetros de qualidade para a devolução destes
recursos.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
